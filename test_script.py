import json
import subprocess
import decord

from video_parser import VideoParser


def extract_frame_types(video_path):
    ffprobe_cmd = [
        "ffprobe",
        "-v",
        "quiet",
        "-print_format",
        "json",
        "-show_entries",
        "frame=pict_type",
        "-select_streams",
        "v",
        video_path,
    ]

    output = subprocess.check_output(ffprobe_cmd)
    data = json.loads(output)

    frame_types = [frame["pict_type"] for frame in data["frames"]]

    return frame_types


# video_file = "/media/nas2/graph_sim_data/video_advanced_splicing/original/0.mp4"
# video_file = "/media/nas2/graph_sim_data/video_advanced_splicing/manipulated/0.mp4"
# video_file = "/media/nas2/graph_sim_data/video_visible_aug/original/0.mp4"
# video_file = "/media/nas2/graph_sim_data/video_visible_aug/manipulated/0.mp4"
# video_file = "/media/nas2/Datasets/VideoSham-adobe-research/processed_videos/0061_processed.mp4"
# video_file = "/media/nas2/Datasets/VideoSham-adobe-research/processed_videos/0062_processed.mp4"
# video_file = "/media/nas2/deepfakes/cvpr/df/Ed - 24KGold.mp4"
video_file = "/media/nas2/deepfakes/cvpr/auth/Ed.mp4"
frame_types = extract_frame_types(video_file)

# for i, frame_type in enumerate(frame_types):
#     print(f"({i}: {frame_type})", end="")
# print()

video_reader = decord.VideoReader(video_file)

video_obj = VideoParser(video_file)
frame_properties = video_obj.get_frame_properties()

frame_idx = 1

parsed_frame = video_obj.get_frame(frame_idx)
print(parsed_frame.height, parsed_frame.width)
macroblocks = parsed_frame.MB

decoded_frame = video_reader[frame_idx].asnumpy()
print(decoded_frame.shape)

for i in macroblocks:
    for j in i:
        if j is None:
            print("None Block encountered")
            raise Exception("None Block encountered")


import matplotlib.patches as patches
import matplotlib.pyplot as plt

fig, ax = plt.subplots(figsize=(22, 22))

frame_width = parsed_frame.width * 16
frame_height = parsed_frame.height * 16
ax.set_xlim(0, frame_width)
ax.set_ylim(0, frame_height)
ax.set_xticks([])
ax.set_yticks([])
ax.set_aspect("equal", "box")

ax.imshow(decoded_frame, alpha=0.7)

for i in range(len(macroblocks)):
    for j in range(len(macroblocks[i])):
        mb = macroblocks[i][j]
        mb_type = "Skip" if mb is None or mb.skip else mb.mb_type
        x, y = i * 16, j * 16
        if "Skip" in mb_type:
            facecolor = "gray"
        else:
            facecolor = "none"
        rect = patches.Rectangle((y, x), 16, 16, linewidth=0.4, edgecolor="r", facecolor=facecolor, alpha=0.7)
        ax.add_patch(rect)

        # Adding text to each macroblock
        mb_type_letter = mb_type[0]  # Assuming the first letter indicates I, P, or B type
        text_x, text_y = y + 4, x + 12  # Add a small offset to position the text within the macroblock
        ax.text(text_x, text_y, mb_type_letter, fontsize=7, color="black")

        if "I_16x16" in mb_type:
            mb_type = "I_16x16"
        mb_partition = mb_type.split("_")[-1]
        if mb_partition in ("16x16", "Skip") or mb_type in ("I_NxN", "I_PCM"):
            continue
        else:
            width_parts, height_parts = list(map(int, mb_partition.split("x")))
        for m in range(0, 16, height_parts):
            for n in range(0, 16, width_parts):
                rect = patches.Rectangle(
                    (y + m, x + n), height_parts, width_parts, linewidth=0.4, edgecolor="b", facecolor="none"
                )
                ax.add_patch(rect)

plt.gca().invert_yaxis()

ax.set_title(f"{parsed_frame.slice_type} Frame")


# plt.figure(figsize=(16, 16))
# plt.imshow(decoded_frame, alpha=1.0)
