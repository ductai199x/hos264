from bitarray import bitarray
from constants import table9_4_a, table9_4_b
from typing import *

# Define these so I can use the .decode method. There are no upper limits on the numbers, but
# 350(+-175) is pretty big, and 240 is the width of 4k video
# This caches the bitarrays for the Exp-Golomb numbers, so we don't have to keep converting them
SE = {
    (-(k + 1) // 2 + 1 if k % 2 == 0 else (k + 1) // 2): bitarray(
        "0" * ((k + 1).bit_length() - 1) + bin(k + 1)[2:]
    )
    for k in range(2000)
}
UE = {k: bitarray("0" * ((k + 1).bit_length() - 1) + bin(k + 1)[2:]) for k in range(2000)}
UE_inv_map = {v.to01(): k for k, v in UE.items()}
SE_inv_map = {v.to01(): k for k, v in SE.items()}

class Bitstream:
    """
    A bitstream object to loosely wrap around the bitarray module.
    Performs decoding operations relevant for h.264 bitstream reading.
    """

    def __init__(self, fhdl=None, maxbytes=None, bytes=None):
        """
        Initializes a bitstream object.
        Object will read from fhdl, an open file handle.
        The object will read no more than maxbytes bytes.
        """
        self.fhdl = fhdl
        self.bytesleft = maxbytes
        if bytes is not None:
            self.stream = bitarray()
            self.stream.frombytes(bytes)
        else:
            self.stream = bitarray()
        self.zeros = 0

    def read(self, nbytes):
        """
        Appends nbytes*8 more bits onto the bitstream.
        Does not return anything.
        """
        if nbytes > self.bytesleft:
            raise IndexError
        for i in range(nbytes):
            x = self.fhdl.read(1)
            if x == b"\x00":
                self.zeros += 1
            else:
                if x == b"\x03" and self.zeros > 1:
                    x = self.fhdl.read(1)
                    self.bytesleft -= 1
                self.zeros = 0
            self.stream.frombytes(x)
        self.bytesleft -= nbytes

    def tell(self) -> Tuple[int, int]:
        end = self.fhdl.tell()
        length = len(self.stream)
        if length % 8 == 0:
            return end - (length // 8), 0
        else:
            return end - (length // 8) - 1, 8 - length % 8

    def more_data(self) -> bool:
        """
        Check if there are more bytes to be read into the bitstream
        """
        return self.bytesleft > 0

    def bytealign(self) -> bitarray:
        """
        Finds next byte boundry in bitstream.
        Truncates stream to start at boundry
        Returns all bits before boundry
        """
        boundry = len(self.stream) % 8
        ret = self.stream[:boundry]
        self.stream = self.stream[boundry:]
        return ret

    def _expgolomb(self, codec) -> list:
        """
        Reads numel expgolomb codewords.
        Truncates stream to start after last codeword
        Returns codeswords decoded using codec
        Raises ValueError if not enough bits for codeword
        """
        ndx = 0
        ret = 0
        if len(self.stream) == 0 or 1 not in self.stream:
            self.read(2)
        while True:
            ndx = self.stream.index(1) * 2 + 1
            if ndx < len(self.stream):
                ret = codec[self.stream[:ndx].to01()]
                break
            else:
                self.read(2)
                continue
            
        self.stream = self.stream[ndx:]
        return ret

    def _decode(self, bitarr) -> int:
        rem = 8 - (len(bitarr) % 8)
        ret = int.from_bytes(bitarr.tobytes(), byteorder="big")
        return ret >> rem - 1

    def ue(self, numel=1) -> Union[int, list[int]]:
        """
        Decodes numel syntax elements using unsigned expgolomb coding.
        Truncates stream to start after last syntax element.
        """
        ret = []
        while len(ret) < numel:
            ret.append(self._expgolomb(UE_inv_map))
        return ret[0] if numel == 1 else ret

    def se(self, numel=1) -> Union[int, list[int]]:
        """
        Decodes numel syntax elements using signed expgolomb coding.
        Truncates stream to start after last syntax element.
        """
        ret = []
        while len(ret) < numel:
            ret.append(self._expgolomb(SE_inv_map))
        return ret[0] if numel == 1 else ret
    
    def me(self, chroma_array_type, numel=1) -> Union[int, list[int]]:
        """
        Decodes numel syntax elements using modified expgolomb coding.
        Truncates stream to start after last syntax element.
        """
        assert chroma_array_type in (0, 1, 2, 3), "Invalid chroma_array_type, must be 0, 1, 2, or 3"
        ret = []
        while len(ret) < numel:
            try:
                code_num = self._expgolomb(UE_inv_map)
                if chroma_array_type in (0, 3):
                    ret.append(table9_4_b[code_num])
                else:
                    ret.append(table9_4_a[code_num])
            except IndexError:
                self.read(2)
        return ret[0] if numel == 1 else ret
    
    def te(self, range: int, numel=1) -> Union[int, list[int]]:
        """
        Decodes numel syntax elements using truncated expgolomb coding.
        Truncates stream to start after last syntax element.
        """
        assert range > 0, "range must be positive"
        ret = []
        while len(ret) < numel:
            try:
                if range > 1:
                    ret.append(self._expgolomb(UE_inv_map))
                else:
                    ret.append(1 - self.u(1))
            except IndexError:
                self.read(2)
        return ret[0] if numel == 1 else ret

    def ae(self, golomb_order) -> int:
        """
        Read an exponential Golomb-coded value from the bitstream.
        """
        code_num = 0
        leading_zeros = -1
        while not (self.u(1)):
            leading_zeros += 1
        for i in range(golomb_order):
            code_num |= self.u(1) << (golomb_order - i - 1)
        return (1 << leading_zeros) - 1 + code_num

    def u(self, numbits) -> int:
        """
        Decodes a numbits-bit fixed length syntax element.
        Returns the bits packed into an int, MSB first.
        Truncates stream to start after last syntax element.
        """
        while len(self.stream) < numbits:
            try:
                self.read(1)
            except IndexError:
                raise IndexError("Insufficient data in the bitstream.")
        ret = 0
        for k in range(numbits):
            ret <<= 1
            ret |= self.stream.pop(0)
        return ret

    def close(self) -> bitarray:
        """
        Byte-aligns the stream.
        Rewinds the file pointer to the first unused byte.
        """
        ext = self.bytealign()
        rewind = -len(self.stream) // 8
        self.fhdl.seek(rewind, 1)
        return ext

    def __repr__(self):
        return f"Bitstream: fhdl_pos={self.tell()}, stream={self.stream.to01()}"


######################################################################
# Original implementation of ue() and se() according to the standard:
"""
def ue(self, numel=1):
        "
        Decodes numel syntax elements using unsigned expgolomb coding.
        Truncates stream to start after last syntax element.
        "
        ret = []
        while len(ret) < numel:
            try:
                leading_zeros = 0
                while True:
                    if len(self.stream) < 8:
                        self.read(1)
                    if not self.stream.pop(0):
                        leading_zeros += 1
                    else:
                        break

                code_num = (1 << leading_zeros) - 1 + self.u(leading_zeros)
                ret.append(code_num)
            except IndexError:
                self.read(2)
        return ret[0] if numel == 1 else ret

def se(self, numel=1):
        "
        Decodes numel syntax elements using signed expgolomb coding.
        Truncates stream to start after last syntax element.
        "
        ret = self.ue(numel)
        if isinstance(ret, int):
            ret = [ret]
        ret = [((value + 1) >> 1) * (-1 if (value & 1) == 0 else 1) for value in ret]
        return ret[0] if numel == 1 else ret
"""
