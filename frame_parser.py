from typing import *

import numpy as np

from cabac import CabacDecoder
from cavlc import CavlcDecoder
from constants import *
from pps_dataclass import PPS
from slice_header_dataclass import SliceHeader
from slice_parser import SliceParser
from sps_dataclass import SPS


def bits_consumed(s1, s2):
    return (s2[0] - s1[0]) * 8 + s2[1] - s1[1]


class FrameParser:
    def __init__(
        self,
        video_parser,  # VideoParser object
        slice_size: int,
        slice_header: SliceHeader,
    ):
        self.fhdl = video_parser.fhdl
        self.slice_size = slice_size
        self.slice_start = self.fhdl.tell()
        self.slice_end = self.slice_start + self.slice_size
        self.pps: PPS = video_parser.PPS[slice_header.pic_parameter_set_id]
        self.sps: SPS = video_parser.SPS[self.pps.seq_parameter_set_id]
        self.slice_header = slice_header
        self.cabac = self.pps.entropy_coding_mode_flag
        self.mbaff = self.sps.mb_adaptive_frame_field_flag
        self.slice_type = self.slice_header.slice_type
        self.slice = SliceParser(slice_header, self.sps, self.pps)
        self.residual_block = self.residual_block_cabac if self.cabac else self.residual_block_cavlc

        if self.cabac:
            if self.slice_type not in ("I", "SI"):
                self.decoder = CabacDecoder(
                    self.fhdl, self.slice, self.slice_size, self.slice_header.cabac_init_idc
                )
            else:
                self.decoder = CabacDecoder(self.fhdl, self.slice, self.slice_size)
        else:
            self.decoder = CavlcDecoder(self.fhdl, self.slice, self.slice_size)

        self.map_unit_to_slice_group_map = None
        self.mb_to_slice_group_map = None

        self.macroblocks_to_slice_group_map()  # need to run this first before everything

    def bits_from_mb_start(self):
        assert self.mb_start_pos is not None
        return bits_consumed(self.mb_start_pos, self.decoder.stream.tell())

    def next_mb_address(self, curr_mb_addr: int) -> int:
        i = curr_mb_addr + 1
        while (
            i < self.pic_size_in_map_units
            and self.mb_to_slice_group_map[i] != self.mb_to_slice_group_map[curr_mb_addr]
        ):
            i += 1
        return i

    def macroblocks_to_slice_group_map(self):
        pic_width_in_mbs = self.sps.pic_width_in_mbs_minus1 + 1
        frame_height_in_mbs = (2 - self.sps.frame_mbs_only_flag) * (
            self.sps.pic_height_in_map_units_minus1 + 1
        )
        pic_height_in_mbs = frame_height_in_mbs // (1 + self.slice_header.field_pic_flag)
        pic_size_in_map_units = pic_width_in_mbs * pic_height_in_mbs

        num_slice_group = self.pps.num_slice_groups_minus1 + 1

        slice_group_change_dir_flag = self.pps.slice_group_change_direction_flag

        map_units_in_slice_group0 = min(
            self.slice_header.slice_group_change_cycle * (self.pps.slice_group_change_rate_minus1 + 1),
            pic_size_in_map_units,
        )

        size_of_upper_left_group = (
            (pic_size_in_map_units - map_units_in_slice_group0)
            if slice_group_change_dir_flag
            else map_units_in_slice_group0
        )

        self.pic_size_in_map_units = pic_size_in_map_units
        self.map_unit_to_slice_group_map = [0] * pic_size_in_map_units
        self.mb_to_slice_group_map = [0] * pic_size_in_map_units

        if self.pps.num_slice_groups_minus1 == 0:
            pass  # all slice groups are the same
        else:
            if self.pps.slice_group_map_type == 0:
                # interleaved slice group map type
                i = 0
                while i < pic_size_in_map_units:
                    i_group = 0
                    while i_group <= num_slice_group and i < pic_size_in_map_units:
                        run_length = self.pps.run_length_minus1[i_group] + 1
                        for j in range(run_length):
                            if i + j < pic_size_in_map_units:
                                self.map_unit_to_slice_group_map[i + j] = i_group
                            else:
                                break
                        i_group += 1
                        i += run_length
            elif self.pps.slice_group_map_type == 1:
                # dispersed slice group map type
                for i in range(pic_size_in_map_units):
                    self.map_unit_to_slice_group_map[i] = (
                        (i % pic_width_in_mbs) + (i // pic_width_in_mbs) * num_slice_group // 2
                    ) % num_slice_group
            elif self.pps.slice_group_map_type == 2:
                # foreground with left-over slice group map type
                for i in range(pic_size_in_map_units):
                    self.map_unit_to_slice_group_map[i] = self.pps.num_slice_groups_minus1
                for i_group in range(self.pps.num_slice_groups_minus1 - 1, -1, -1):
                    y_top_left = self.pps.top_left[i_group] // pic_width_in_mbs
                    x_top_left = self.pps.top_left[i_group] % pic_width_in_mbs
                    y_bottom_right = self.pps.bottom_right[i_group] // pic_width_in_mbs
                    x_bottom_right = self.pps.bottom_right[i_group] % pic_width_in_mbs
                    for y in range(y_top_left, y_bottom_right + 1):
                        for x in range(x_top_left, x_bottom_right + 1):
                            self.map_unit_to_slice_group_map[y * pic_width_in_mbs + x] = i_group
            elif self.pps.slice_group_map_type == 3:
                # box-out slice group map types
                for i in range(pic_size_in_map_units):
                    self.map_unit_to_slice_group_map[i] = 1
                x = (pic_width_in_mbs - slice_group_change_dir_flag) // 2
                y = (pic_height_in_mbs - slice_group_change_dir_flag) // 2
                left_bound, top_bound = x, y
                right_bound, bottom_bound = x, y
                x_dir, y_dir = slice_group_change_dir_flag - 1, slice_group_change_dir_flag
                k = 0
                while k < map_units_in_slice_group0:
                    if self.map_unit_to_slice_group_map[y * pic_size_in_map_units + x] == 1:
                        self.map_unit_to_slice_group_map[y * pic_size_in_map_units + x] = 0
                        k += 1

                    if x_dir == -1 and x == left_bound:
                        left_bound = max(left_bound - 1, 0)
                        x = left_bound
                        x_dir, y_dir = 0, 2 * slice_group_change_dir_flag - 1
                    elif x_dir == 1 and x == right_bound:
                        right_bound = min(right_bound + 1, pic_size_in_map_units - 1)
                        x = right_bound
                        x_dir, y_dir = 0, 1 - 2 * slice_group_change_dir_flag
                    elif y_dir == -1 and y == top_bound:
                        top_bound = max(top_bound - 1, 0)
                        y = top_bound
                        x_dir, y_dir = 1 - 2 * slice_group_change_dir_flag, 0
                    elif y_dir == 1 and y == bottom_bound:
                        bottom_bound = min(bottom_bound + 1, pic_height_in_mbs - 1)
                        y = bottom_bound
                        x_dir, y_dir = 2 * slice_group_change_dir_flag - 1, 0
                    else:
                        x, y = x + x_dir, y + y_dir
            elif self.pps.slice_group_map_type == 4:
                # raster scan slice group map types
                for i in range(pic_size_in_map_units):
                    if i < size_of_upper_left_group:
                        self.map_unit_to_slice_group_map[i] = slice_group_change_dir_flag
                    else:
                        self.map_unit_to_slice_group_map[i] = 1 - slice_group_change_dir_flag
            elif self.pps.slice_group_map_type == 5:
                # wipe slice group map types
                k = 0
                for j in range(pic_width_in_mbs):
                    for i in range(pic_height_in_mbs):
                        if k < size_of_upper_left_group:
                            self.map_unit_to_slice_group_map[
                                i * pic_width_in_mbs + j
                            ] = slice_group_change_dir_flag
                        else:
                            self.map_unit_to_slice_group_map[i * pic_width_in_mbs + j] = (
                                1 - slice_group_change_dir_flag
                            )
                        k += 1
            elif self.pps.slice_group_map_type == 6:
                # explicit slice group map type
                for i in range(pic_size_in_map_units):
                    self.map_unit_to_slice_group_map[i] = self.pps.slice_group_id[i]
            else:
                raise ValueError("Invalid slice group map type")

            # map macroblock to slice group map:
            if self.sps.frame_mbs_only_flag or self.slice_header.field_pic_flag:
                for i in range(pic_size_in_map_units):
                    self.mb_to_slice_group_map[i] = self.map_unit_to_slice_group_map[i]
            else:
                if self.mbaff:
                    for i in range(pic_size_in_map_units):
                        self.mb_to_slice_group_map[i] = self.map_unit_to_slice_group_map[i // 2]
                else:
                    for i in range(pic_size_in_map_units):
                        self.mb_to_slice_group_map[i] = self.map_unit_to_slice_group_map[
                            (i // (2 * pic_width_in_mbs)) * pic_width_in_mbs + (i % pic_width_in_mbs)
                        ]

    # 7.3.4
    def get_macroblocks(self):
        more_data_flag = True
        prev_mb_skipped = False
        mb_skip_run = 0
        mb_count = 0
        # calvc does not require the slice data to be byte aligned, so we need to check where we were at the end of the slice header, restore the bitstream to that exact position before continuing.
        slice_header_end_pos, bits_left_in_byte = self.slice_header.slice_end_file_position
        curr_pos = self.fhdl.tell()
        if self.cabac == 0:
            if slice_header_end_pos < curr_pos:
                self.fhdl.seek(-1, 1)
            if bits_left_in_byte > 0:
                self.decoder.stream.u(bits_left_in_byte)
        while more_data_flag:
            self.mb_start_pos = self.decoder.stream.tell()
            block = self.slice.add_block()
            mb_count += 1
            if self.slice_type not in ["I", "SI"]:
                if self.cabac == 0:  # calvc
                    mb_skip_run = self.decoder.decode_mb_skip_run()
                    prev_mb_skipped = mb_skip_run > 0
                    for i in range(mb_skip_run):
                        # set the current block to be skipped
                        block.skip = True
                        block.mb_type = f"{self.slice_type[0]}_Skip"
                        self.slice.curr_mb_addr = self.next_mb_address(self.slice.curr_mb_addr)
                        # decrement by self.slice.curr_mb_addr 1 because it will be incremented by 1 in the add_block function
                        if self.slice.curr_mb_addr == self.pic_size_in_map_units:
                            more_data_flag = False
                            break
                        self.slice.curr_mb_addr -= 1
                        # add a new block
                        block = self.slice.add_block()
                        mb_count += 1
                    if mb_skip_run > 0:
                        self.mb_start_pos = self.decoder.stream.tell()
                else:  # cabac
                    mb_skip_flag = self.decoder.decode_mb_skip()
                    if mb_skip_flag:
                        block.mb_type = None
                        block.skip = True
                    more_data_flag = not mb_skip_flag
            if more_data_flag:
                mb_field_decoding_flag = 0
                if self.mbaff and (
                    self.slice.curr_mb_addr % 2 == 0 or (self.slice.curr_mb_addr % 2 == 1 and prev_mb_skipped)
                ):
                    mb_field_decoding_flag = self.decoder.decode_mb_field()
                block.field = mb_field_decoding_flag
                self.macroblock_layer()
            if self.cabac == 0:  # cavlc
                if mb_count < self.pic_size_in_map_units:
                    more_data_flag = self.decoder.more_data()
                else:
                    more_data_flag = False
            else:  # cabac
                if self.slice_type not in ["I", "SI"]:
                    prev_mb_skipped = mb_skip_flag
                if self.mbaff and self.slice.curr_mb_addr % 2 == 0:
                    more_data_flag = 1
                else:
                    eos_flag = self.decoder.decode_EOS()
                    more_data_flag = not eos_flag

            self.mb_end_pos = self.decoder.stream.tell()
            block.size = bits_consumed(self.mb_start_pos, self.mb_end_pos)
        #     print(
        #         f"more_data_flag: {more_data_flag}, mb_type: {block.mb_type}, mb_addr: {block.addr}, mb_CBP: {block.CBP}, [{self.mb_start_pos}, {self.mb_end_pos}, {block.size}]"
        #     )
        # print(f"mb_count: {mb_count}")

        return self.slice

    # 7.3.5
    def macroblock_layer(self):
        block = self.slice.block
        block.mb_type = self.decoder.decode_mb_type()
        if block.mb_type == "I_PCM":
            tmp = self.decoder.stream.bytealign()
            if not sum(tmp) == 0:
                raise ValueError(f"bytealign: {tmp} - pcm_alignment_zero_bits encountered non-zero bit")
            depth = self.sps.bit_depth_luma_minus8 + 8
            block.luma = np.array([self.decoder.stream.u(depth) for i in range(256)])
            depth = self.sps.bit_depth_chroma_minus8 + 8
            # TODO-9 use chroma format to determine how many chroma samples are present
            block.chroma = np.array([self.decoder.stream.u(depth) for i in range(2 * 8 * 8)])
        else:
            noSubMbPartSizeLessThan8x8Flag = 1
            if block.mb_type in ["P_8x8", "P_8x8ref0", "B_8x8"]:
                self.sub_mb_pred()  # IMPORTANT
                for i in range(4):
                    if not block.sub_mb_type(i) == "B_Direct_8x8":
                        if block.NumSubMbPart(i) > 1:
                            noSubMbPartSizeLessThan8x8Flag = 0
                    elif not self.sps.direct_8x8_inference_flag:
                        noSubMbPartSizeLessThan8x8Flag = 0
            else:
                if self.pps.transform_8x8_mode_flag and block.mb_type == "I_NxN":
                    block.transform_8x8_flag = self.decoder.decode_transform8x8_flag()
                self.mb_pred()  # IMPORTANT
            if block.MbPartPredMode() != "Intra_16x16":
                block.CBP = self.decoder.decode_CBP()
                if (
                    block.CBPLuma > 0
                    and self.pps.transform_8x8_mode_flag
                    and block.mb_type != "I_NxN"
                    and noSubMbPartSizeLessThan8x8Flag
                    and (self.sps.direct_8x8_inference_flag or block.mb_type != "B_Direct_16x16")
                ):
                    block.transform_8x8_flag = self.decoder.decode_transform8x8_flag()
            if block.MbPartPredMode() == "Intra_16x16" or block.CBPLuma > 0 or block.CBPChroma > 0:
                block.mb_qp_delta = self.decoder.decode_mb_qp_delta()
                self.residual(0, 15)  # IMPORTANT
        return block

    # 7.3.5.1
    def mb_pred(self):
        block = self.slice.block
        # TODO-9: replace string equivalence checks with transfor_8x8_flag and mb_type[0]=='I'
        # TODO-3: read how to use prev and rem pred mode, so that these pred_mode can be stored direct
        if block.MbPartPredMode()[0] == "I":
            if block.MbPartPredMode() == "Intra_4x4":
                block.sb_pred_dir = np.zeros(16, dtype=np.uint8)
                for i in range(16):
                    prev_mode_flag = self.decoder.decode_prev_mode_flag()
                    block.sb_pred_dir[i] = self.decoder.decode_rem_intra4x4_pred_mode(prev_mode_flag, i)
            if block.MbPartPredMode() == "Intra_8x8":
                block.sb_pred_dir = np.zeros(4, dtype=np.uint8)
                for i in range(4):
                    prev_mode_flag = self.decoder.decode_prev_mode_flag()
                    block.sb_pred_dir[i] = self.decoder.decode_rem_intra8x8_pred_mode(
                        prev_mode_flag, block.field, i
                    )
            if self.slice.chroma_array_type in [1, 2]:
                block.intra_chroma_pred_mode = self.decoder.decode_intra_chroma_pred_mode()
            else:
                block.intra_chroma_pred_mode = 0  # DC
        # Then it must be a P or B Block. It's not b_direct, right?
        elif block.MbPartPredMode() != "Direct":
            num_parts = block.NumMbPart()
            block.ref_idx_l0 = np.zeros(num_parts, dtype=np.int32)
            block.ref_idx_l1 = np.zeros(num_parts, dtype=np.int32)
            block.mvd_l0 = np.zeros((num_parts, 4, 2), dtype=np.int32)
            block.mvd_l1 = np.zeros((num_parts, 4, 2), dtype=np.int32)
            for i in range(num_parts):
                if (
                    self.slice_header.num_ref_idx_l0_active_minus1 > 0
                    or block.field != self.slice.field_pic_flag
                ) and block.MbPartPredMode(i) != "Pred_L1":
                    block.ref_idx_l0[i] = self.decoder.decode_ref_idx(i, 0)
            for i in range(num_parts):
                if (
                    self.slice_header.num_ref_idx_l1_active_minus1 > 0
                    or block.field != self.slice.field_pic_flag
                ) and block.MbPartPredMode(i) != "Pred_L0":
                    block.ref_idx_l1[i] = self.decoder.decode_ref_idx(i, 1)
            for i in range(num_parts):
                if block.MbPartPredMode(i) != "Pred_L1":
                    # horizontal then vertical, check annex A
                    block.mvd_l0[i][0][0] = self.decoder.decode_mvd(i, 0, 0, 0)
                    block.mvd_l0[i][0][1] = self.decoder.decode_mvd(i, 0, 0, 1)
            for i in range(num_parts):
                if block.MbPartPredMode(i) != "Pred_L0":
                    # horizontal then vertical, check annex A
                    block.mvd_l1[i][0][0] = self.decoder.decode_mvd(i, 0, 1, 0)
                    block.mvd_l1[i][0][1] = self.decoder.decode_mvd(i, 0, 1, 1)

    # 7.3.5.2
    def sub_mb_pred(self):
        block = self.slice.block
        sub_mb_type = [None] * 4
        for i in range(4):
            sub_mb_type[i] = self.decoder.decode_sub_mb_type()
        block.set_sub_mb_type(sub_mb_type)
        block.ref_idx_l0 = np.zeros(4)
        block.ref_idx_l1 = np.zeros(4)
        block.mvd_l0 = np.zeros([4, 4, 2])
        block.mvd_l1 = np.zeros([4, 4, 2])
        for i in range(4):
            if self.slice_header.num_ref_idx_l0_active_minus1 > 0 or block.field != self.slice.field_pic_flag:
                if (
                    block.mb_type != "P_8x8ref0"
                    and block.sub_mb_type(i) != "B_Direct_8x8"
                    and block.subMbPredMode(i) != "Pred_L1"
                ):
                    block.ref_idx_l0[i] = self.decoder.decode_ref_idx(i, 0)
        for i in range(4):
            if self.slice_header.num_ref_idx_l1_active_minus1 > 0 or block.field != self.slice.field_pic_flag:
                if block.sub_mb_type(i) != "B_Direct_8x8" and block.subMbPredMode(i) != "Pred_L0":
                    block.ref_idx_l1[i] = self.decoder.decode_ref_idx(i, 1)
        for i in range(4):
            if block.sub_mb_type(i) != "B_Direct_8x8" and block.subMbPredMode(i) != "Pred_L1":
                for k in range(block.NumSubMbPart(i)):
                    block.mvd_l0[i][k][0] = self.decoder.decode_mvd(i, k, 0, 0)
                    block.mvd_l0[i][k][1] = self.decoder.decode_mvd(i, k, 0, 1)
        for i in range(4):
            if block.sub_mb_type(i) != "B_Direct_8x8" and block.subMbPredMode(i) != "Pred_L0":
                for k in range(block.NumSubMbPart(i)):
                    block.mvd_l1[i][k][0] = self.decoder.decode_mvd(i, k, 1, 0)
                    block.mvd_l1[i][k][1] = self.decoder.decode_mvd(i, k, 1, 1)

    # 7.3.5.3
    def residual(self, startIdx, endIdx):
        block = self.slice.block
        self.residual_luma(startIdx, endIdx)
        if self.slice.chroma_array_type in [1, 2]:
            numC8x8 = self.slice.numC8x8
            block.chromaDCLevel = np.zeros([2, 4 * numC8x8], dtype=np.int32)
            for icbcr in range(2):
                if block.CBPChroma & 3 and startIdx == 0:
                    flag, coeffs = self.residual_block(
                        0,
                        4 * numC8x8 - 1,
                        maxNumCoeff=4 * numC8x8,
                        ctxBlockCat=3,
                        residual_type="ChromaDCLevel",
                        icbcr=icbcr,
                    )
                    block.CBF["cbcr"][icbcr]["DC"] = flag
                    block.chromaDCLevel[icbcr] = coeffs
                else:
                    block.chromaDCLevel[icbcr] = np.zeros(4 * numC8x8)
            block.chromaACLevel = np.zeros([2, 4 * numC8x8, 15], dtype=np.int32)
            for icbcr in range(2):
                for i8 in range(numC8x8):
                    for i4 in range(4):
                        if block.CBPChroma & 2:
                            flag, coeffs = self.residual_block(
                                max(0, startIdx - 1),
                                endIdx - 1,
                                maxNumCoeff=15,
                                ctxBlockCat=4,
                                residual_type="ChromaACLevel",
                                blk_idx=i8 * 4 + i4,
                                icbcr=icbcr,
                            )
                            block.CBF["cbcr"][icbcr]["AC"][i8 * 4 + i4] = flag
                            block.chromaACLevel[icbcr][i8 * 4 + i4] = coeffs
                        else:
                            block.chromaACLevel[icbcr][i8 * 4 + i4] = np.zeros(15)
        elif self.slice.chroma_array_type == 3:
            self.residual_chroma(startIdx, endIdx, chnl=0)
            self.residual_chroma(startIdx, endIdx, chnl=1)

    # 7.3.5.3.1
    def residual_luma(self, startIdx, endIdx):
        """
        Using Table 9-42 to get the maxNumCoeff and ctxBlockCat
        """
        block = self.slice.block
        block.DCLevel = np.zeros(16, dtype=np.int32)
        block.ACLevel = np.zeros((16, 15), dtype=np.int32)
        block.level4x4 = np.zeros((16, 16), dtype=np.int32)
        block.level8x8 = np.zeros((4, 64), dtype=np.int32)
        if startIdx == 0 and block.MbPartPredMode() == "Intra_16x16":
            flag, coeffs = self.residual_block(
                0,
                15,
                maxNumCoeff=16,
                ctxBlockCat=0,
                residual_type="Intra16x16DCLevel",
            )
            block.CBF["y"]["DC"] = flag
            block.DCLevel = coeffs
        for i8 in range(4):
            if not block.transform_8x8_flag or not self.cabac:
                for i4 in range(4):
                    if block.CBPLuma & (1 << i8):
                        if block.MbPartPredMode() == "Intra_16x16":
                            flag, coeffs = self.residual_block(
                                max(0, startIdx - 1),
                                endIdx - 1,
                                maxNumCoeff=15,
                                ctxBlockCat=1,
                                residual_type="Intra16x16ACLevel",
                                blk_idx=i8 * 4 + i4,
                            )
                            block.CBF["y"]["AC"][i8 * 4 + i4] = flag
                            block.ACLevel[i8 * 4 + i4] = coeffs
                        else:
                            flag, coeffs = self.residual_block(
                                startIdx,
                                endIdx,
                                maxNumCoeff=16,
                                ctxBlockCat=2,
                                residual_type="LumaLevel4x4",
                                blk_idx=i8 * 4 + i4,
                            )
                            block.CBF["y"]["level4"][i8 * 4 + i4] = flag
                            block.level4x4[i8 * 4 + i4] = coeffs
                    elif block.MbPartPredMode() == "Intra_16x16":
                        block.ACLevel[i8 * 4 + i4] = np.zeros(15)
                    else:
                        block.level4x4[i8 * 4 + i4] = np.zeros(16)
                    if block.transform_8x8_flag and not self.cabac:
                        for k in range(16):
                            block.level8x8[i8][4 * k + i4] = block.level4x4[i8 * 4 + i4][k]
            elif block.CBPLuma & (1 << i8):
                flag, coeffs = self.residual_block(
                    4 * startIdx,
                    4 * endIdx + 3,
                    maxNumCoeff=64,
                    ctxBlockCat=5,
                    residual_type="LumaLevel8x8",
                    blk_idx=i8,
                )
                block.CBF["y"]["level8"][i8] = flag
                block.level8x8[i8] = coeffs
            else:
                block.level8x8[i8] = np.zeros(64)

        block.DC_decoded = self.inverse_transform_dc_levels(block.DCLevel).flatten()
        block.level8x8_decoded = np.zeros((4, 8, 8), dtype=np.int32)
        block.level4x4_decoded = np.zeros((16, 4, 4), dtype=np.int32)
        for i in range(16):
            block.level4x4_decoded[i] = (
                self.inverse_transform_residual_block(block.level4x4[i].reshape(4, 4), self.slice.qpy)
                + block.DC_decoded[i]
            )
        for i in range(4):
            block.level8x8_decoded[i] = self.inverse_transform_residual_block(
                block.level8x8[i].reshape(8, 8), self.slice.qpy
            )

    # 7.3.5.3.1 chroma alternate
    def residual_chroma(self, startIdx, endIdx, channel=0):
        channel_str = "Cb" if channel == 0 else "Cr"
        aug = 4 * channel
        block = self.slice.block
        block.DCLevel = np.zeros(16, dtype=np.int32)
        block.ACLevel = np.zeros((16, 15), dtype=np.int32)
        if channel == 0:
            block.cblevel4x4 = np.zeros((16, 16), dtype=np.int32)
            block.cblevel8x8 = np.zeros((4, 64), dtype=np.int32)
            block.cblevel8x8_decoded = np.zeros((4, 8, 8), dtype=np.int32)
            block.cblevel4x4_decoded = np.zeros((16, 4, 4), dtype=np.int32)
            level4x4 = block.cblevel4x4
            level8x8 = block.cblevel8x8
            level4x4_decoded = block.cblevel4x4_decoded
            level8x8_decoded = block.cblevel8x8_decoded
        else:
            block.crlevel4x4 = np.zeros((16, 16), dtype=np.int32)
            block.crlevel8x8 = np.zeros((4, 64), dtype=np.int32)
            block.crlevel8x8_decoded = np.zeros((4, 8, 8), dtype=np.int32)
            block.crlevel4x4_decoded = np.zeros((16, 4, 4), dtype=np.int32)
            level4x4 = block.crlevel4x4
            level8x8 = block.crlevel8x8
            level4x4_decoded = block.crlevel4x4_decoded
            level8x8_decoded = block.crlevel8x8_decoded

        if startIdx == 0 and block.MbPartPredMode() == "Intra_16x16":
            flag, coeffs = self.residual_block(
                0,
                15,
                maxNumCoeff=16,
                ctxBlockCat=6 + aug,
                residual_type=f"{channel_str}Intra16x16DCLevel",
                icbcr=channel,
            )
            block.CBF["cbcr"][channel]["DC"] = flag
            block.DCLevel = coeffs
        for i8 in range(4):
            if not block.transform_size_8x8_flag or not self.cabac:
                for i4 in range(4):
                    if block.CBPLuma & (1 << i8):
                        if block.MbPartPredMode() == "Intra_16x16":
                            flag, coeffs = self.residual_block(
                                max(0, startIdx - 1),
                                endIdx - 1,
                                maxNumCoeff=15,
                                ctxBlockCat=7 + aug,
                                residual_type=f"{channel_str}Intra16x16ACLevel",
                                blk_idx=i8 * i4 + i4,
                                icbcr=channel,
                            )
                            block.CBP["cbcr"][channel]["AC"][i8 * 4 + i4] = flag
                            block.ACLevel[i8 * 4 + i4] = coeffs
                        else:
                            flag, coeffs = self.residual_block(
                                startIdx,
                                endIdx,
                                maxNumCoeff=16,
                                ctxBlockCat=8 + aug,
                                residual_type=f"{channel_str}Level4x4",
                                blk_idx=i8 * 4 + i4,
                                icbcr=channel,
                            )
                            block.CBP["cbcr"][channel]["level4"][i8 * 4 + i4] = flag
                            level4x4[i8 * 4 + i4] = coeffs
                    elif block.MbPartPredMode() == "Intra_16x16":
                        block.ACLevel[i8 * 4 + i4] = np.zeros(15)
                    else:
                        level4x4[i8 * 4 + i4] = np.zeros(16)
                    if block.transform8x8_flag and not self.cabac:
                        for k in range(16):
                            level8x8[i8][4 * k + i4] = level4x4[i8 * 4 + i4][k]
            elif block.CBPLuma & (1 << i8):
                flag, coeffs = self.residual_block(
                    4 * startIdx,
                    4 * endIdx + 3,
                    maxNumCoeff=64,
                    ctxBlockCat=9 + aug,
                    residual_type=f"{channel_str}Level8x8",
                    blk_idx=i8,
                    icbcr=channel,
                )
                block.CBP["cbcr"][channel]["level8"][i8] = flag
                level8x8[i8] = coeffs
            else:
                level8x8[i8] = np.zeros(64)

        for i in range(16):
            level4x4_decoded[i] = self.inverse_transform_residual_block(
                level4x4[i].reshape(4, 4), self.slice.qpy
            )
        for i in range(4):
            level8x8_decoded[i] = self.inverse_transform_residual_block(
                level8x8[i].reshape(8, 8), self.slice.qpy
            )

    # 7.3.5.3.2
    def residual_block_cavlc(
        self, startIdx, endIdx, maxNumCoeff, ctxBlockCat, residual_type, blk_idx=None, icbcr=0
    ):
        assert isinstance(self.decoder, CavlcDecoder)
        # print(
        #     f"residual_block_cavlc: startIdx={startIdx}, endIdx={endIdx}, maxNumCoeff={maxNumCoeff}, ctxBlockCat={ctxBlockCat}, residual_type={residual_type}, blk_idx={blk_idx}, icbcr={icbcr}"
        # )
        trailing_ones, total_coeff, nC = self.decoder.decode_coeffs_token(
            maxNumCoeff, ctxBlockCat, residual_type, blk_idx, icbcr
        )

        coeff_level = np.zeros(maxNumCoeff, dtype=np.int32)
        level_values = np.zeros(maxNumCoeff, dtype=np.int32)
        if total_coeff > 0:
            if total_coeff > 10 and trailing_ones < 3:
                suffix_length = 1
            else:
                suffix_length = 0
            for i in range(total_coeff):
                if i < trailing_ones:
                    trailing_ones_sign_flag = self.decoder.decode_trailing_ones_sign_flag()
                    level_values[i] = 1 - 2 * trailing_ones_sign_flag
                else:
                    level_prefix = self.decoder.decode_level_prefix()
                    level_code = min(15, level_prefix) << suffix_length
                    if suffix_length > 0 or level_prefix >= 14:
                        level_suffix = self.decoder.decode_level_suffix(level_prefix, suffix_length)
                        level_code += level_suffix
                    if level_prefix >= 15 and suffix_length == 0:
                        level_code += 15
                    if level_prefix >= 16:
                        level_code += (1 << (level_prefix - 3)) - 4096
                    if i == trailing_ones and trailing_ones < 3:
                        level_code += 2
                    if level_code % 2 == 0:
                        level_values[i] = (level_code + 2) >> 1
                    else:
                        level_values[i] = (-level_code - 1) >> 1
                    if suffix_length == 0:
                        suffix_length = 1
                    if abs(level_values[i]) > (3 << (suffix_length - 1)) and suffix_length < 6:
                        suffix_length += 1

            run_val = self.decoder.decode_run_val(total_coeff, maxNumCoeff)
            coeff_num = -1
            for j in range(total_coeff - 1, -1, -1):
                coeff_num += run_val[j] + 1
                coeff_level[startIdx + coeff_num] = level_values[j]

        coded_block_flag = int(total_coeff > 0)
        return coded_block_flag, coeff_level

    # 7.3.5.3.3
    def residual_block_cabac(
        self, startIdx, endIdx, maxNumCoeff, ctxBlockCat, residual_type, blk_idx=None, icbcr=0
    ):
        assert isinstance(self.decoder, CabacDecoder)
        # print(
        #    f"residual_block_cabac: startIdx={startIdx}, endIdx={endIdx}, maxNumCoeff={maxNumCoeff}, ctxBlockCat={ctxBlockCat}, residual_type={residual_type}, blk_idx={blk_idx}, icbcr={icbcr}"
        # )
        coded_block_flag = 1
        coeff_map = np.zeros(maxNumCoeff, dtype=np.int32)
        coeff_level = np.zeros(maxNumCoeff, dtype=np.int32)
        if maxNumCoeff != 64 or self.slice.chroma_array_type == 3:
            coded_block_flag = self.decoder.decode_coded_block_flag(ctxBlockCat, blk_idx, icbcr)
        if coded_block_flag:
            coeff_map = self.decoder.decode_significant_coeff_map(ctxBlockCat, startIdx, endIdx + 1)
            coeff_level = self.decoder.decode_level_coeffs(ctxBlockCat, coeff_map)
        return coded_block_flag, coeff_level.astype(np.int32)

    ########################################################
    # Inverse Transform DC Level Coefficients
    def inverse_transform_dc_levels(self, coeffs: np.ndarray) -> np.ndarray:
        assert len(coeffs.flatten()) == 16, "DC Coefficients must be 4x4"
        if len(coeffs.shape) == 1:
            coeffs = coeffs.reshape(4, 4)
        return self.hadamard_transform(coeffs).T

    def hadamard_transform(self, matrix):
        hadamard_matrix = np.array([[1, 1, 1, 1], [1, -1, 1, -1], [1, 1, -1, -1], [1, -1, -1, 1]])

        return np.round((hadamard_matrix @ matrix @ hadamard_matrix.T) / 2).astype(int)

    ########################################################
    # Inverse Transform Luma and Chroma (4x4 and 8x8) Blocks
    def inverse_transform_residual_block(self, block: np.ndarray, qp: int) -> np.ndarray:
        if len(block.shape) == 2:
            block = block.flatten()

        assert len(block) in (16, 64), "Block must be 4x4 or 8x8"

        if self.sps.seq_scaling_matrix_present_flag:
            raise NotImplementedError("Non-default scaling matrices not yet implemented")

        block_type = 0 if len(block) == 16 else 1

        if block_type == 0:
            level_scale = norm_adjust_4x4 * default_scaling_list_4x4
            zigzag_order = zigzag_order_4x4
        else:
            level_scale = norm_adjust_8x8 * default_scaling_list_8x8
            zigzag_order = zigzag_order_8x8

        block = block[zigzag_order]

        qp_mod = qp % 6
        qp_div = qp // 6
        scaling_matrix = level_scale[qp_mod]

        if block_type == 0:
            if qp >= 24:
                block = block * (scaling_matrix << (qp_div - 4))
            else:
                block = block * ((scaling_matrix + 2 ** (3 - qp_div)) >> (4 - qp_div))
        else:
            if qp >= 36:
                block = block * (scaling_matrix << (qp_div - 6))
            else:
                block = block * ((scaling_matrix + 2 ** (5 - qp_div)) >> (6 - qp_div))

        if block_type == 0:
            return self.inverse_transform_4x4(block)
        else:
            return self.inverse_transform_8x8(block)

    def inverse_transform_8x8(self, block: np.ndarray) -> np.ndarray:
        e = np.zeros(block.shape, dtype=np.int32)
        f = np.zeros(block.shape, dtype=np.int32)
        g = np.zeros(block.shape, dtype=np.int32)
        h = np.zeros(block.shape, dtype=np.int32)
        k = np.zeros(block.shape, dtype=np.int32)
        m = np.zeros(block.shape, dtype=np.int32)
        # Horizontal inverse transform
        for i in range(8):
            e[i, 0] = block[i, 0] + block[i, 4]
            e[i, 1] = -block[i, 3] + block[i, 5] - block[i, 7] - (block[i, 7] >> 1)
            e[i, 2] = block[i, 0] - block[i, 4]
            e[i, 3] = block[i, 1] + block[i, 7] - block[i, 3] - (block[i, 3] >> 1)
            e[i, 4] = (block[i, 2] >> 1) - block[i, 6]
            e[i, 5] = -block[i, 1] + block[i, 7] + block[i, 5] + (block[i, 5] >> 1)
            e[i, 6] = block[i, 2] + (block[i, 6] >> 1)
            e[i, 7] = block[i, 3] + block[i, 5] + block[i, 1] + (block[i, 1] >> 1)

            f[i, 0] = e[i, 0] + e[i, 6]
            f[i, 1] = e[i, 1] + (e[i, 7] >> 2)
            f[i, 2] = e[i, 2] + e[i, 4]
            f[i, 3] = e[i, 3] + (e[i, 5] >> 2)
            f[i, 4] = e[i, 2] - e[i, 4]
            f[i, 5] = (e[i, 3] >> 2) - e[i, 5]
            f[i, 6] = e[i, 0] - e[i, 6]
            f[i, 7] = e[i, 7] - (e[i, 1] >> 2)

            g[i, 0] = f[i, 0] + f[i, 7]
            g[i, 1] = f[i, 2] + f[i, 5]
            g[i, 2] = f[i, 4] + f[i, 3]
            g[i, 3] = f[i, 6] + f[i, 1]
            g[i, 4] = f[i, 6] - f[i, 1]
            g[i, 5] = f[i, 4] - f[i, 3]
            g[i, 6] = f[i, 2] - f[i, 5]
            g[i, 7] = f[i, 0] - f[i, 7]

        # Vertical inverse transform
        for j in range(8):
            h[0, j] = g[0, j] + g[4, j]
            h[1, j] = -g[3, j] + g[5, j] - g[7, j] - (g[7, j] >> 1)
            h[2, j] = g[0, j] - g[4, j]
            h[3, j] = g[1, j] + g[7, j] - g[3, j] - (g[3, j] >> 1)
            h[4, j] = (g[2, j] >> 1) - g[6, j]
            h[5, j] = -g[1, j] + g[7, j] + g[5, j] + (g[5, j] >> 1)
            h[6, j] = g[2, j] + (g[6, j] >> 1)
            h[7, j] = g[3, j] + g[5, j] + g[1, j] + (g[1, j] >> 1)

            k[0, j] = h[0, j] + h[6, j]
            k[1, j] = h[1, j] + (h[7, j] >> 2)
            k[2, j] = h[2, j] + h[4, j]
            k[3, j] = h[3, j] + (h[5, j] >> 2)
            k[4, j] = h[2, j] - h[4, j]
            k[5, j] = (h[3, j] >> 2) - h[5, j]
            k[6, j] = h[0, j] - h[6, j]
            k[7, j] = h[7, j] - (h[1, j] >> 2)

            m[0, j] = k[0, j] + k[7, j]
            m[1, j] = k[2, j] + k[5, j]
            m[2, j] = k[4, j] + k[3, j]
            m[3, j] = k[6, j] + k[1, j]
            m[4, j] = k[6, j] - k[1, j]
            m[5, j] = k[4, j] - k[3, j]
            m[6, j] = k[2, j] - k[5, j]
            m[7, j] = k[0, j] - k[7, j]

        return (m + 32) >> 6

    def inverse_transform_4x4(self, block: np.ndarray) -> np.ndarray:
        # Initialize intermediate matrices
        e = np.zeros((4, 4), dtype=int)
        f = np.zeros((4, 4), dtype=int)
        g = np.zeros((4, 4), dtype=int)
        h = np.zeros((4, 4), dtype=int)

        for i in range(4):
            e[i, 0] = block[i, 0] + block[i, 2]
            e[i, 1] = block[i, 0] - block[i, 2]
            e[i, 2] = (block[i, 1] >> 1) - block[i, 3]
            e[i, 3] = block[i, 1] + (block[i, 3] >> 1)

            f[i, 0] = e[i, 0] + e[i, 3]
            f[i, 1] = e[i, 1] + e[i, 2]
            f[i, 2] = e[i, 1] - e[i, 2]
            f[i, 3] = e[i, 0] - e[i, 3]

        for j in range(4):
            g[0, j] = f[0, j] + f[2, j]
            g[1, j] = f[0, j] - f[2, j]
            g[2, j] = (f[1, j] >> 1) - f[3, j]
            g[3, j] = f[1, j] + (f[3, j] >> 1)

            h[0, j] = g[0, j] + g[3, j]
            h[1, j] = g[1, j] + g[2, j]
            h[2, j] = g[1, j] - g[2, j]
            h[3, j] = g[0, j] - g[3, j]

        return (h + 32) >> 6
