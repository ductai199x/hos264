from typing import *
import numpy as np

from pps_dataclass import PPS
from slice_header_dataclass import SliceHeader
from sps_dataclass import SPS

MAP_16x16_0_TRANSF = np.array([[0, 1, 4, 5], [2, 3, 6, 7], [8, 9, 12, 13], [10, 11, 14, 15]])
MAP_16x16_1_TRANSF = np.array([[0, 1], [2, 3]])
MAP_16x8 = np.array([[0, 0], [1, 1]])
MAP_8x16 = np.array([[0, 1], [0, 1]])
MAP_8x8 = np.array([[0, 1], [2, 3]])

SUBMAP_8x8 = np.array([[0, 0], [0, 0]])
SUBMAP_8x4 = np.array([[0, 0], [1, 1]])
SUBMAP_4x8 = np.array([[0, 1], [0, 1]])
SUBMAP_4x4 = np.array([[0, 1], [2, 3]])


class MacroblockParser:
    def __init__(self, slice_type):
        self.skip = False
        self.slice_type = slice_type
        self.size = 0
        self.intra_chroma_pred_mode = None
        self.field = 0  # mbaff
        self.transform_8x8_flag = 0
        self.prev = None
        self.mb_qp_delta = 0
        self.addr = None
        self.sb_pred_dir = None
        self.ref_idx_l0 = None
        self.ref_idx_l1 = None
        self.mvd_l0 = None
        self.mvd_l1 = None
        self._mb_type = None
        self.DCLevel = None
        self.ACLevel = None
        self.cbDC = None
        self.cbAC = None
        self.crDC = None
        self.crAC = None
        self.chromaDCLevel = None
        self.chromaACLevel = None
        self.level4x4 = None
        self.level8x8 = None
        self.cblevel4x4 = None
        self.cblevel8x8 = None
        self.crlevel4x4 = None
        self.crlevel8x8 = None
        self.CBF = {
            "y": {"DC": None, "AC": [None] * 16, "level4": [None] * 16, "level8": [None] * 4},
            "cbcr": [
                {"DC": None, "AC": [None] * 16, "level4": [None] * 16, "level8": [None] * 4},
                {"DC": None, "AC": [None] * 16, "level4": [None] * 16, "level8": [None] * 4},
            ],
        }
        self.CBP = None

    def __repr__(self):
        return f"Macroblock(Type: {self.mb_type})"

    def __str__(self):
        return repr(self)

    def print(self):
        print(f"Type: {self.mb_type}, at {self.addr}")
        if not self.MbPartPredMode == "Intra_16x16":
            print(f"Transform_8x8_flag: {self.transform_8x8_flag}")
            # print(f"Pred mode: {self.sb_pred_dir}")
        print(f"chroma pred mode: {self.intra_chroma_pred_mode}")
        print(f"Coded Block Pattern: {self.CBP}")
        print(f"QP delta: {self.mb_qp_delta}")
        if self.MbPartPredMode == "Intra_16x61":
            print(f"DC Levels: {self.DCLevel}")
            print(f"AC Levels: {self.ACLevel}")
        else:
            if self.transform_8x8_flag:
                print(f"Level 8x8: {self.level8x8}")
            else:
                print(f"Level 4x4: {self.level4x4}")
        print(f"DC Chroma Levels: {self.chromaDCLevel}")
        print(f"AC Chroma Levels: {self.chromaACLevel}")

    @property
    def mb_type(self):
        return self._mb_type

    @mb_type.setter
    def mb_type(self, mbtype):
        self._mb_type = mbtype
        if mbtype is None:
            self.skip = True
            self._mb_type = f"{self.slice_type}_Skip"
            # TODO: set defaults
        elif "I_16x16" in mbtype:
            tmp = mbtype.split("_")
            self.CBP = 15 * int(tmp[-1]) + (int(tmp[-2]) << 4)
            self.intra16x16predmode = int(tmp[-3])

    @property
    def CBPLuma(self):
        return self.CBP % 16

    @property
    def CBPChroma(self):
        return self.CBP // 16

    def NumMbPart(self):
        mbtype = self.mb_type
        if "16x16" in mbtype or "skip" in mbtype:
            return 1
        if "8x16" in mbtype or "16x8" in mbtype:
            return 2
        if "8x8" in mbtype:
            return 4

    def MBpartShape(self):
        mbtype = self.mb_type
        if "16x16" in mbtype:
            return 16, 16
        elif "16x8" in mbtype:
            return 8, 16
        elif "8x16" in mbtype:
            return 16, 8
        elif "8x8" in mbtype:
            return 8, 8
        raise ValueError(f"Macroblock type {mbtype} does not appear to have valid macroblock partition shape")

    def MbPartPredMode(self, ndx=0):
        if self.mb_type[0] == "I":
            if self.mb_type == "I_NxN":
                return "Intra_8x8" if self.transform_8x8_flag else "Intra_4x4"
            else:
                return "Intra_16x16"
        elif self.mb_type[0] == "P":
            return "Pred_L0"
        elif self.mb_type[0] == "B":
            pred = self.mb_type.split("_")[ndx + 1]
            if pred == "L0":
                return "Pred_L0"
            if pred == "L1":
                return "Pred_L1"
            if pred == "Bi":
                return "BiPred"
            if pred in ["Direct", "Skip"]:
                return "Direct"
            return "NA"
        raise ValueError(f"Macroblock type '{self.mb_type}' has no defined mbPartPredMode")

    def sub_mb_type(self, ndx):
        val = self._sub_mb_type[ndx]
        return val

    def set_sub_mb_type(self, val):
        self._sub_mb_type = val

    def NumSubMbPart(self, ndx):
        sub_mb_type = self.sub_mb_type(ndx)
        if sub_mb_type == "B_Direct_8x8":
            return 4
        if sub_mb_type.endswith("8x8"):
            return 1
        elif sub_mb_type.endswith(("4x8", "8x4")):
            return 2
        elif sub_mb_type.endswith("4x4"):
            return 4

    def subMBpartShape(self, ndx):
        mbtype = self.sub_mb_type(ndx)
        if "4x4" in mbtype:
            return 4, 4
        elif "4x8" in mbtype:
            return 8, 4
        elif "8x4" in mbtype:
            return 4, 8
        elif "8x8" in mbtype:
            return 8, 8
        raise ValueError(
            f"SubMacroblock type {mbtype} does not appear to have valid submacroblock partition shape"
        )

    def subMbPredMode(self, ndx):
        sub_mb_type = self._sub_mb_type[ndx].split("_")[1]
        # TODO-9: raise error if no value is to be returned
        if sub_mb_type == "Direct":
            return "Direct"
        if sub_mb_type == "L0":
            return "Pred_L0"
        if sub_mb_type == "L1":
            return "Pred_L1"
        if sub_mb_type == "Bi":
            return "BiPred"

    def get_blockmap(self):
        mbtype = self.mb_type
        if mbtype[0] == "I":
            return None, None
        if mbtype in ["P_8x8", "P_8x8ref0", "B_8x8", "B_Skip", "B_Direct_16x16"]:
            return SUBMAP_4x4
        else:
            h, w = self.MBpartShape()
            if h == 16 and w == 16:
                return SUBMAP_8x8
            if h == 16 and w == 8:
                return SUBMAP_4x8
            if h == 8 and w == 16:
                return SUBMAP_8x4
        raise ValueError(f"Blockmaps are not defined for macroblock type {mbtype}")

    def get_submap(self, ndx):
        mbtype = self.mb_type
        if mbtype in ["B_Skip", "B_Direct_16x16"]:
            return SUBMAP_4x4
        if mbtype in ["P_8x8", "P_8x8ref0", "B_8x8"]:
            h, w = self.subMBpartShape(ndx)
            if h == 8 and w == 8:
                return SUBMAP_8x8
            if h == 8 and w == 4:
                return SUBMAP_4x8
            if h == 4 and w == 8:
                return SUBMAP_8x4
            if h == 4 and w == 4:
                return SUBMAP_4x4
        if mbtype[0] in ["P", "B"]:
            return SUBMAP_8x8
        raise ValueError(f"Submacroblocks are not defined for macroblock type {mbtype}")


class SliceParser:
    def __init__(self, slice_header: SliceHeader, sps: SPS, pps: PPS):
        self.slice_header = slice_header
        self.pps = pps
        self.sps = sps
        self.cabac = self.pps.entropy_coding_mode_flag
        self.field_pic_flag = self.slice_header.field_pic_flag
        self.mbaff = self.sps.mb_adaptive_frame_field_flag and not self.slice_header.field_pic_flag
        # if not self.cabac:
        #     raise NotImplementedError(f"CAVLC decoder not yet implemented")
        if self.field_pic_flag:
            raise NotImplementedError(f"Field-coded slices not yet implemented")
        if self.mbaff:
            raise NotImplementedError(f"Field-coded slices not yet implemented")
        self.curr_mb_addr = self.slice_header.first_mb_in_slice * (1 + self.mbaff) - 1
        self.slice_type = self.slice_header.slice_type
        self.height = int((self.sps.pic_height_in_map_units_minus1 + 1) / (1 + self.field_pic_flag))
        self.width = self.sps.pic_width_in_mbs_minus1 + 1
        self.MB: List[List[Union[MacroblockParser, None]]] = [None] * self.height
        for i in range(self.height):
            self.MB[i] = [None] * self.width
        self.qpy = 26 + self.pps.pic_init_qp_minus26 + self.slice_header.slice_qp_delta
        self.chroma_array_type = 0 if self.sps.separate_colour_plane_flag else self.sps.chroma_format_idc
        subWidthC = 2 if self.sps.chroma_format_idc < 3 else 1
        subHeightC = 1 if self.sps.chroma_format_idc > 1 else 2
        self.numC8x8 = 4 // (subWidthC * subHeightC)
        self.MBA = None
        self.MBB = None
        self.block = None
        self.prev = None

    def __getitem__(self, idx: Union[int, Tuple[int, int]]) -> MacroblockParser:
        if isinstance(idx, int):
            return self.MB[idx % self.width][idx // self.height]
        else:
            return self.MB[idx[0]][idx[1]]

    def __repr__(self):
        return f"Slice(Type={self.slice_type}, At address {self.curr_mb_addr_2d})"

    @property
    def curr_mb_addr_2d(self) -> Tuple[int, int]:
        y = self.curr_mb_addr // self.width
        x = self.curr_mb_addr % self.width
        return y, x

    def add_block(self, field=False) -> MacroblockParser:
        self.prev = self.block
        self.block = MacroblockParser(self.slice_type)
        self.block.prev = self.prev
        self.curr_mb_addr += 1
        self.block.addr = self.curr_mb_addr
        y, x = self.curr_mb_addr_2d
        self.MB[y][x] = self.block
        self.MBA = self.get_MBA()
        self.MBB = self.get_MBB()
        return self.block

    def get_MBA(self):
        """
        WARNING: field macroblocks are not supported at this time
        MBA is the address of the macroblock to the left of the current macroblock.
        """
        y, x = self.curr_mb_addr_2d
        if x == 0:
            return None
        return self.MB[y][x - 1]

    def get_MBB(self):
        """
        WARNING: field macroblocks are not supported at this time
        MBB is the address of the macroblock above the current macroblock.
        """
        y, x = self.curr_mb_addr_2d
        if y == 0:
            return None
        return self.MB[y - 1][x]

    def get_neighbors(self, ctxBlockCat, ndx=None):
        if ctxBlockCat in [0, 6, 10] or ctxBlockCat == 3:
            # The above conditions are logically different, but functionally identical
            # 6.4.11.1
            return self.MBA, self.MBB
        elif ctxBlockCat in [1, 2, 7, 8, 11, 12]:
            # 1 intra_16x16 AC
            # 2 level4x4 residual
            # 4 chroma AC
            # 7 cb intra_16x16 4x4 residual
            # 11 cr intra_16x16 4x4 residual
            # 8 cb level4x4 residual
            # 12 cr level4x4 residual

            # 6.4.11.4
            mbmap = MAP_16x16_0_TRANSF
            mba, ndxa = self.get_subMBA(ndx, mbmap)
            mbb, ndxb = self.get_subMBB(ndx, mbmap)
            return mba, ndxa[0], mbb, ndxb[0]
        elif ctxBlockCat == 4:
            # 4 chroma AC
            mbmap = MAP_8x8
            if self.chroma_array_type == 2:
                mbmap = SUBMAP_8x4
            mba, ndxa = self.get_subMBA(ndx, mbmap)
            mbb, ndxb = self.get_subMBB(ndx, mbmap)
            return mba, ndxa[0], mbb, ndxb[0]
        elif ctxBlockCat in [5, 9, 13]:
            # 6.4.11.{2,3}
            mbmap = MAP_8x8
            mba, ndxa = self.get_subMBA(ndx, mbmap)
            mbb, ndxb = self.get_subMBB(ndx, mbmap)
            return mba, ndxa[0], mbb, ndxb[0]
        raise ValueError(f"Unrecognized value {ctxBlockCat} for ctxBlockCat")

    def get_subMBA(self, ndx, blockmap):
        """
        WARNING: field macroblocks are not supported at this time
        """
        by, bx = np.where(blockmap == ndx)
        retndx = blockmap[by, bx - 1]
        if bx == 0:
            return self.MBA, retndx
        return self.block, retndx

    def get_subMBB(self, ndx, blockmap):
        """
        WARNING: field macroblocks are not supported at this time
        """
        by, bx = np.where(blockmap == ndx)
        retndx = blockmap[by - 1, bx]
        if by == 0:
            return self.MBB, retndx
        return self.block, retndx

    def get_neighbor_blocks(self, ndx, subndx=None):
        if subndx is None:
            return self.get_MBApart(ndx), self.get_MBBpart(ndx)
        return self.get_subMBApart(ndx, subndx), self.get_subMBBpart(ndx, subndx)

    def get_MBApart(self, ndx):
        MBA = self.block
        blockmap = MBA.get_blockmap()
        by, bx = np.where(blockmap == ndx)
        by = by[0]
        bx = bx[0]
        if not bx:
            MBA = self.MBA
        return MBA, (by, bx - 1)

    def get_MBBpart(self, ndx):
        MBB = self.block
        blockmap = MBB.get_blockmap()
        by, bx = np.where(blockmap == ndx)
        by = by[0]
        bx = bx[0]
        if not by:
            MBB = self.MBB
        return MBB, (by - 1, bx)

    def get_subMBApart(self, ndx, subndx):
        block = self.block
        blockmap = block.get_blockmap()
        submap = block.get_submap(ndx)
        by, bx = np.where(blockmap == ndx)
        by = by[0]
        bx = bx[0]
        sy, sx = np.where(submap == subndx)
        sy = sy[0]
        sx = sx[0] - 1
        if sx < 0:
            bx -= 1
        if bx < 0:
            block = self.MBA
        return block, (by, bx), (sy, sx)

    def get_subMBBpart(self, ndx, subndx):
        block = self.block
        blockmap = block.get_blockmap()
        submap = block.get_submap(ndx)
        by, bx = np.where(blockmap == ndx)
        by = by[0]
        bx = bx[0]
        sy, sx = np.where(submap == subndx)
        sy = sy[0] - 1
        sx = sx[0]
        if sy < 0:
            by -= 1
        if by < 0:
            block = self.MBB
        return block, (by, bx), (sy, sx)
