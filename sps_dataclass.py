from dataclasses import dataclass, field
from typing import Optional


@dataclass
class SPS:
    """Sequence Parameter Set dataclass."""

    profile_idc: int
    constraint_flag_and_reserved_zero_bits: int  # 6 bits constraint flags + 2 bits reserved
    level_idc: int
    seq_parameter_set_id: int
    log2_max_frame_num_minus4: int
    pic_order_cnt_type: int
    max_num_ref_frames: int
    gaps_in_frame_num_value_allowed_flag: int
    pic_width_in_mbs_minus1: int
    pic_height_in_map_units_minus1: int
    frame_mbs_only_flag: int
    direct_8x8_inference_flag: int
    frame_cropping_flag: int
    vui_parameters_present_flag: int
    separate_colour_plane_flag: Optional[int] = field(default=None)
    chroma_format_idc: Optional[int] = field(default=None)
    bit_depth_luma_minus8: Optional[int] = field(default=None)
    bit_depth_chroma_minus8: Optional[int] = field(default=None)
    qpprime_y_zero_transform_bypass_flag: Optional[int] = field(default=None)
    seq_scaling_matrix_present_flag: Optional[int] = field(default=None)
    log2_max_pic_order_cnt_lsb_minus4: Optional[int] = field(default=None)
    delta_pic_order_always_zero_flag: Optional[int] = field(default=None)
    offset_for_non_ref_pic: Optional[int] = field(default=None)
    offset_for_top_to_bottom_field: Optional[int] = field(default=None)
    num_ref_frames_in_pic_order_cnt_cycle: Optional[int] = field(default=None)
    offset_for_ref_frame: Optional[int] = field(default=None)
    mb_adaptive_frame_field_flag: Optional[int] = field(default=None)
    frame_crop_left_offset: Optional[int] = field(default=None)
    frame_crop_right_offset: Optional[int] = field(default=None)
    frame_crop_top_offset: Optional[int] = field(default=None)
    frame_crop_bottom_offset: Optional[int] = field(default=None)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)
