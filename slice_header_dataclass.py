from dataclasses import dataclass, field
from typing import *


@dataclass
class SliceHeader:
    """Slice header dataclass."""

    slice_start_file_position: int
    slice_end_file_position: Union[int, Tuple[int, int]]
    slice_size: int
    nal_ref_idc: int
    nal_unit_type: int
    first_mb_in_slice: int
    slice_type: str
    frame_num: int
    pic_parameter_set_id: Optional[int] = field(default=None)
    colour_plane_id: Optional[int] = field(default=None)
    field_pic_flag: Optional[int] = field(default=None)
    bottom_field_flag: Optional[int] = field(default=None)
    idr_pic_id: Optional[int] = field(default=None)
    pic_order_cnt_lsb: Optional[int] = field(default=None)
    delta_pic_order_cnt_bottom: Optional[int] = field(default=None)
    delta_pic_order_cnt_0: Optional[int] = field(default=None)
    delta_pic_order_cnt_1: Optional[int] = field(default=None)
    redundant_pic_cnt_present_flag: Optional[int] = field(default=None)
    redundant_pic_cnt: Optional[int] = field(default=None)
    direct_spatial_mv_pred_flag: Optional[int] = field(default=None)
    num_ref_idx_active_override_flag: Optional[int] = field(default=None)
    num_ref_idx_l0_active_minus1: Optional[int] = field(default=None)
    num_ref_idx_l1_active_minus1: Optional[int] = field(default=None)
    ref_pic_list_reordering: Optional[int] = field(default=None)
    adaptive_ref_pic_marking_mode_flag: Optional[int] = field(default=None)
    no_output_of_prior_pics_flag: Optional[int] = field(default=None)
    long_term_reference_flag: Optional[int] = field(default=None)
    difference_of_pic_nums_minus1: Optional[List[int]] = field(default=None)
    long_term_pic_num: Optional[List[int]] = field(default=None)
    long_term_frame_idx: Optional[List[int]] = field(default=None)
    max_long_term_frame_idx_plus1: Optional[List[int]] = field(default=None)
    cabac_init_idc: Optional[int] = field(default=None)
    slice_qp_delta: Optional[int] = field(default=None)
    sp_for_switch_flag: Optional[int] = field(default=None)
    slice_qs_delta: Optional[int] = field(default=None)
    disable_deblocking_filter_idc: Optional[int] = field(default=None)
    deblocking_file_pointer_position: Optional[int] = field(default=None)
    slice_alpha_c0_offset_div2: Optional[int] = field(default=None)
    slice_beta_offset_div2: Optional[int] = field(default=None)
    slice_group_change_cycle: Optional[int] = field(default=None)
    luma_log2_weight_denom: Optional[int] = field(default=None)
    chroma_log2_weight_denom: Optional[int] = field(default=None)
    luma_weight_l0: Optional[int] = field(default=None)
    luma_offset_l0: Optional[int] = field(default=None)
    chroma_weight_l0: Optional[int] = field(default=None)
    chroma_offset_l0: Optional[int] = field(default=None)
    luma_weight_l1: Optional[int] = field(default=None)
    luma_offset_l1: Optional[int] = field(default=None)
    chroma_weight_l1: Optional[int] = field(default=None)
    chroma_offset_l1: Optional[int] = field(default=None)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)
