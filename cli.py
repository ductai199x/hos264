#!/usr/bin/env python3

import argparse as ap
import os

import numpy as np

from video_parser import VideoParser


def parse_args():
    parse = ap.ArgumentParser(description="A tool for examining coding properties of videos")
    parse.add_argument("-t", "--type", action="store_true", help="Report frame(slice) type for each frame")
    parse.add_argument("-s", "--size", action="store_true", help="Report endoded frame size for each frame")
    parse.add_argument("-q", "--qp", action="store_true", help="Report Quantization parameter for each frame")
    parse.add_argument(
        "--order",
        choices=["decode", "display"],
        default="display",
        help="Choose the order in which frame information should be displayed",
    )
    parse.add_argument("media", nargs="+", help="Path to one or more videos to investigate.")
    parse.add_argument("--logfile", default=None, help="Path to save log file(s).")
    return parse.parse_args()


def report_frame_data(video_parser: VideoParser, t, s, q, order=True, logfile=None):
    frame_data = video_parser.get_frame_properties(order)
    properties = []
    output_data = []

    if t:
        properties.append("Frame Type")
        output_data.append(frame_data["frame_types"])
    if s:
        properties.append("Encoded Size")
        output_data.append(frame_data["frame_sizes"])
    if q:
        properties.append("Quantization Parameter")
        output_data.append(frame_data["frame_quant_params"])
    output_data = list(zip(*output_data))
    output = ",".join(properties) + "\n"
    for row in output_data:
        output += ",".join(map(str, row)) + "\n"
    if logfile is None:
        print(output)
    else:
        with open(logfile, "w") as f:
            f.write(output)


def main():
    args = parse_args()
    if args.logfile is not None:
        logfile = lambda x: f"{args.logfile}.{os.path.splitext(os.path.basename(x))[0]}"
    else:
        logfile = lambda x: None
    order = args.order == "display"
    for fpath in args.media:
        video_parser = VideoParser(fpath)
        if args.type or args.size or args.qp:
            report_frame_data(video_parser, args.type, args.size, args.qp, order, logfile(fpath))


if __name__ == "__main__":
    main()
