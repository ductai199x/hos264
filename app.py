import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import decord
import time
import os
import sys
import gradio as gr
from typing import *


from video_parser import VideoParser
from slice_parser import SliceParser
from colors import colors_to_rgb


def generate_overlay(
    parsed_frame: SliceParser,
    decoded_frame: np.ndarray,
    macroblock_type_fontsize=0.25,
    macroblock_type_colors=["red", "aero", "green", "gray"],  # I, P, B, Skip
    macroblock_edge_color="black",
    macroblock_edge_linewidth=1,
    macroblock_partition_edge_color="dark_blue",
    macroblock_partition_edge_linewidth=1,
):
    h, w, c = decoded_frame.shape
    macroblock_size = 16
    macroblocks = parsed_frame.MB
    macroblock_type_colors = dict(
        zip(["I", "P", "B", "S"], [colors_to_rgb[c] for c in macroblock_type_colors])
    )

    ovl_frame = decoded_frame.copy()
    ovl_text_frame = np.ones_like(decoded_frame) * 255
    ovl_edge_frame = np.ones_like(decoded_frame) * 255
    ovl_partition_frame = np.zeros_like(decoded_frame)

    for i in range(len(macroblocks)):
        for j in range(len(macroblocks[i])):
            mb = macroblocks[i][j]
            x, y = j * macroblock_size, i * macroblock_size
            top_left = (x, y)
            bottom_right = (x + macroblock_size, y + macroblock_size)

            mb_type = "S" if mb is None else mb.mb_type
            mb_type_ = "S" if mb is None or mb.skip else mb_type[0]
            mb_color = macroblock_type_colors[mb_type_]

            cv2.rectangle(ovl_frame, top_left, bottom_right, mb_color, -1)

            if mb_type_ == "I":
                mb_partition = mb.MbPartPredMode().split("_")[-1]
            else:
                mb_partition = mb_type.split("_")[-1]
            if mb_partition in ("8x16", "16x8", "8x8", "4x8", "8x4"):
                width_parts, height_parts = list(map(int, mb_partition.split("x")))
                for m in range(height_parts, 16 - 1, height_parts):
                    cv2.line(
                        ovl_partition_frame,
                        (x + 2, y + m),
                        (x + macroblock_size - 2, y + m),
                        colors_to_rgb[macroblock_partition_edge_color],
                        macroblock_partition_edge_linewidth,
                    )  # Draw horizontal lines
                for n in range(width_parts, 16 - 1, width_parts):
                    cv2.line(
                        ovl_partition_frame,
                        (x + n, y + 2),
                        (x + n, y + macroblock_size - 2),
                        colors_to_rgb[macroblock_partition_edge_color],
                        macroblock_partition_edge_linewidth,
                    )  # Draw vertical lines

            cv2.putText(
                ovl_text_frame,
                mb_type_,
                (x + 5, y + macroblock_size - 7),
                cv2.FONT_HERSHEY_SIMPLEX,
                macroblock_type_fontsize,
                colors_to_rgb["black"],
                1,
            )

    # Draw the grid lines on the blended annotated image
    for y in range(0, h, macroblock_size):
        cv2.line(
            ovl_edge_frame, (0, y), (w, y), colors_to_rgb[macroblock_edge_color], macroblock_edge_linewidth
        )  # Draw horizontal lines

    for x in range(0, w, macroblock_size):
        cv2.line(
            ovl_edge_frame, (x, 0), (x, h), colors_to_rgb[macroblock_edge_color], macroblock_edge_linewidth
        )  # Draw vertical lines

    return ovl_frame, ovl_text_frame, ovl_edge_frame, ovl_partition_frame


def generate_output(
    title: str,
    parsed_frame: SliceParser,
    decoded_frame: np.ndarray,
    is_show_overlay=True,
    overlay_alpha=0.25,
    is_show_macroblock_text=True,
    macroblock_type_fontsize=0.25,
    macroblock_type_colors=["red", "aero", "green", "gray"],  # I, P, B, Skip
    is_show_macroblock_edge=True,
    macroblock_edge_color="black",
    macroblock_edge_linewidth=1,
    is_show_macroblock_partitions=True,
    macroblock_partition_edge_color="dark_brown",
    macroblock_partition_edge_linewidth=1,
):
    ovl_frame, ovl_text_frame, ovl_edge_frame, ovl_partition_frame = generate_overlay(
        parsed_frame,
        decoded_frame,
        macroblock_type_fontsize,
        macroblock_type_colors,
        macroblock_edge_color,
        macroblock_edge_linewidth,
        macroblock_partition_edge_color,
        macroblock_partition_edge_linewidth,
    )

    result_frame = decoded_frame
    if is_show_overlay:
        result_frame = cv2.addWeighted(result_frame, 1 - overlay_alpha, ovl_frame, overlay_alpha, 0)
    if is_show_macroblock_edge:
        result_frame[ovl_edge_frame == 0] = ovl_edge_frame[ovl_edge_frame == 0] + 60
    if is_show_macroblock_partitions:
        result_frame[ovl_partition_frame > 0] = ovl_partition_frame[ovl_partition_frame > 0]
    if is_show_macroblock_text:
        result_frame[ovl_text_frame == 0] = ovl_text_frame[ovl_text_frame == 0] + 60

    if len(title) > 0:
        title_size, _ = cv2.getTextSize(title, cv2.FONT_HERSHEY_COMPLEX, 1, 2)
        title_width, title_height = title_size
        title_image = np.ones((title_height + 20, result_frame.shape[1], 3), dtype=np.uint8) * 255
        title_position = ((result_frame.shape[1] - title_width) // 2, title_height + 10)
        cv2.putText(
            title_image,
            title,
            title_position,
            cv2.FONT_HERSHEY_COMPLEX,
            1,
            colors_to_rgb["black"],
            2,
            cv2.LINE_AA,
        )

        result_frame = cv2.vconcat([title_image, result_frame])

    return result_frame


def get_data(video_file: str, frame_idx: int, return_frame_types: bool = False):
    if video_file is None or not os.path.exists(video_file):
        raise FileNotFoundError(f"Video file {video_file} does not exist")
    t1 = time.time()
    video_obj = VideoParser(video_file)
    fp = video_obj.get_frame_properties()
    pf = video_obj.get_frame(frame_idx)

    video_reader = decord.VideoReader(video_file)
    df = video_reader[frame_idx].asnumpy()

    frame_types = ", ".join([f"({i}: {t})" for i, t in enumerate(fp["frame_types"])][:100]) + "..."
    print(f"get_data() takes {time.time() - t1:.5f} seconds")
    if return_frame_types:
        return fp, pf, df, frame_types, None
    else:
        return fp, pf, df, None


def process(
    frame_properties,
    frame_num,
    parsed_frame,
    decoded_frame,
    is_show_overlay,
    is_show_macroblock_text,
    is_show_macroblock_edge,
    is_show_macroblock_partitions,
    overlay_alpha,
):
    assert frame_num < len(
        frame_properties["frame_types"]
    ), f"Frame Index is out-of-range. Index={frame_num}, Max={len(frame_properties['frame_types']) - 1}"
    t1 = time.time()
    result_frame = generate_output(
        f"Frame {frame_num}, {frame_properties['frame_types'][frame_num]} Type",
        parsed_frame,
        decoded_frame,
        is_show_overlay=is_show_overlay,
        overlay_alpha=overlay_alpha,
        is_show_macroblock_text=is_show_macroblock_text,
        is_show_macroblock_edge=is_show_macroblock_edge,
        is_show_macroblock_partitions=is_show_macroblock_partitions,
    )
    print(f"generate_output() takes {time.time() - t1:.5f} seconds")
    return result_frame


with gr.Blocks() as demo:
    frame_properties, parsed_frame, decoded_frame = gr.State(), gr.State(), gr.State()
    with gr.Row():
        with gr.Column():
            input_video = gr.Video(source="upload", format="mp4", interactive=True)
            input_video.upload(lambda video: video, [input_video], input_video)
        with gr.Column():
            frame_num = gr.Slider(
                label="Frame Index", minimum=0, maximum=100, step=1, value=1, interactive=True
            )
            ckbx_ovl = gr.Checkbox(label="Show Overlay", interactive=True)
            ckbx_ovl_text = gr.Checkbox(label="Show Macroblock Text", interactive=True)
            ckbx_ovl_edge = gr.Checkbox(label="Show Macroblock Edge", interactive=True)
            ckbx_ovl_partitions = gr.Checkbox(label="Show Macroblock Partitions", interactive=True)
            slidr_ovl_alpha = gr.Slider(
                label="Overlay Alpha", minimum=0.0, maximum=1.0, step=0.01, value=0.25, interactive=True
            )
            list_frame_type = gr.Textbox(label="Frame Type", value="")
            btn_submit = gr.Button(value="Run")
    with gr.Row():
        img_result = gr.Image(label="Result")

    with gr.Row():
        gr.Examples(
            examples=[
                "examples/0_manip.mp4",
                "examples/0_orig.mp4",
                "examples/1_manip.mp4",
                "examples/1_orig.mp4",
                "examples/2_manip.mp4",
                "examples/3_manip.mp4",
            ],
            inputs=[input_video],
        )

    input_el_list = [ckbx_ovl, ckbx_ovl_text, ckbx_ovl_edge, ckbx_ovl_partitions, slidr_ovl_alpha]
    for i in input_el_list:
        i.change(
            fn=process,
            inputs=[frame_properties, frame_num, parsed_frame, decoded_frame, *input_el_list],
            outputs=[img_result],
        )

    input_video.change(
        fn=get_data,
        inputs=[input_video, frame_num, gr.State(True)],
        outputs=[frame_properties, parsed_frame, decoded_frame, list_frame_type, img_result],
    ).then(
        process,
        inputs=[frame_properties, frame_num, parsed_frame, decoded_frame, *input_el_list],
        outputs=[img_result],
    )

    btn_submit.click(
        fn=get_data,
        inputs=[input_video, frame_num, gr.State(False)],
        outputs=[frame_properties, parsed_frame, decoded_frame, img_result],
    ).then(
        process,
        inputs=[frame_properties, frame_num, parsed_frame, decoded_frame, *input_el_list],
        outputs=[img_result],
    )

demo.queue(concurrency_count=10, max_size=20).launch(show_error=True, share=True)
