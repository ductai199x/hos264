from dataclasses import dataclass, field
from typing import Optional


@dataclass
class PPS:
    """Picture Parameter Set dataclass."""

    pic_parameter_set_id: int
    seq_parameter_set_id: int
    entropy_coding_mode_flag: int
    bottom_field_pic_order_in_frame_present_flag: Optional[int] = field(default=None)
    num_slice_groups_minus1: Optional[int] = field(default=None)
    slice_group_map_type: Optional[int] = field(default=None)
    run_length_minus1: Optional[int] = field(default=None)
    top_left: Optional[int] = field(default=None)
    bottom_right: Optional[int] = field(default=None)
    slice_group_change_direction_flag: Optional[int] = field(default=None)
    slice_group_change_rate_minus1: Optional[int] = field(default=None)
    pic_size_in_map_units_minus1: Optional[int] = field(default=None)
    slice_group_id: Optional[int] = field(default=None)
    num_ref_idx_l0_default_active_minus1: Optional[int] = field(default=None)
    num_ref_idx_l1_default_active_minus1: Optional[int] = field(default=None)
    weighted_pred_flag: Optional[int] = field(default=None)
    weighted_bipred_idc: Optional[int] = field(default=None)
    pic_init_qp_minus26: Optional[int] = field(default=None)
    pic_init_qs_minus26: Optional[int] = field(default=None)
    chroma_qp_index_offset: Optional[int] = field(default=None)
    deblocking_filter_control_present_flag: Optional[int] = field(default=None)
    constrained_intra_pred_flag: Optional[int] = field(default=None)
    redundant_pic_cnt_present_flag: Optional[int] = field(default=None)
    transform_8x8_mode_flag: Optional[int] = field(default=None)
    pic_scaling_matrix_present_flag: Optional[int] = field(default=None)
    second_chroma_qp_index_offset: Optional[int] = field(default=None)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)
