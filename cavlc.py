import os

import numpy as np
from bitarray import bitarray
from typing import *

from bitstream import Bitstream
from codes import MB_TYPE_I, MB_TYPE_P, MB_TYPE_B, SUBMB_TYPE_P, SUBMB_TYPE_B
from constants import table_9_5, table_9_7_8, table_9_9_a, table_9_9_b, table_9_10

from slice_parser import SliceParser, MacroblockParser

relative = os.path.split(__file__)[0]
rangeTabLPS = np.loadtxt(os.path.join(relative, "rangeTab.gz"), dtype=np.uint8)
transIdxTab = np.loadtxt(os.path.join(relative, "transIdx.gz"), dtype=np.uint8)

MB_TYPE_I_CAVLC = {i: v for i, (k, v) in enumerate(MB_TYPE_I.items())}
MB_TYPE_P_CAVLC = {i: v for i, (k, v) in enumerate(MB_TYPE_P.items())}
MB_TYPE_B_CAVLC = {i: v for i, (k, v) in enumerate(MB_TYPE_B.items())}
SUBMB_TYPE_P_CAVLC = {i: v for i, (k, v) in enumerate(SUBMB_TYPE_P.items())}
SUBMB_TYPE_B_CAVLC = {i: v for i, (k, v) in enumerate(SUBMB_TYPE_B.items())}


class CavlcDecoder:
    def __init__(
        self,
        file_handler,
        slice: SliceParser,
        slice_size,
    ):
        """
        file_handler - An open file handle to the video being decoded
        slice - A Slice object containing the contex of the current frame
        slice_size - The size of the current slice in bytes
        cabac_init - The cabac initialization index parsed form P and B slice headers.
        """
        self.slice = slice
        self.slice_size = slice_size
        self.fhdl = file_handler
        self.bitstream = bitarray()
        self.stream = Bitstream(self.fhdl, self.slice_size)

    @property
    def MBA(self) -> MacroblockParser:
        return self.slice.MBA

    @property
    def MBB(self) -> MacroblockParser:
        return self.slice.MBB

    @property
    def block(self):
        return self.slice.block

    @property
    def prev(self):
        return self.slice.prev

    def more_data(self):
        return self.stream.more_data()

    def decode_mb_skip_run(self):
        return self.stream.ue(1)

    def decode_mb_field(self):
        if self.slice.mbaff:
            return self.slice.slice_header.field_pic_flag
        else:
            return self.stream.u(1)

    def decode_intra_mb(self, ue_mb_type):
        if ue_mb_type > 25:
            raise ValueError(f"Invalid ue_mb_type={ue_mb_type}")
        else:
            return MB_TYPE_I_CAVLC[ue_mb_type]

    def decode_mb_type(self):
        slice_type = self.slice.slice_type
        ue_mb_type = self.stream.ue(1)
        if slice_type == "B":
            if ue_mb_type < 23:
                return MB_TYPE_B_CAVLC[ue_mb_type]
            else:
                ue_mb_type -= 23
                return self.decode_intra_mb(ue_mb_type)
        elif slice_type == "P":
            if ue_mb_type < 5:
                return MB_TYPE_P_CAVLC[ue_mb_type]
            else:
                ue_mb_type -= 5
                return self.decode_intra_mb(ue_mb_type)
        elif slice_type in ("I", "SI"):
            if slice_type == "SI" and ue_mb_type > 0:
                ue_mb_type -= 1
            return self.decode_intra_mb(ue_mb_type)
        
    def decode_sub_mb_type(self):
        slice_type = self.slice.slice_header.slice_type
        ue_sub_mb_type = self.stream.ue(1)
        if slice_type == "B":
            return SUBMB_TYPE_B_CAVLC[ue_sub_mb_type]
        elif slice_type == "P":
            return SUBMB_TYPE_P_CAVLC[ue_sub_mb_type]

    def decode_transform8x8_flag(self):
        return self.stream.u(1)

    def decode_prev_mode_flag(self):
        return self.stream.u(1)

    def decode_rem_intra4x4_pred_mode(self, prev_mode_flag: bool, luma_blk_idx: int):
        MBA = self.MBA
        MBB = self.MBB
        # Derive dcPredModePredictedFlag
        if (
            MBA is None
            or MBB is None
            or (
                MBA is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBA.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
            or (
                MBB is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBB.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
        ):
            dcPredModePredictedFlag = 1
        else:
            dcPredModePredictedFlag = 0

        intraMxMPredModeA = 2  # DC_MODE
        intraMxMPredModeB = 2
        if dcPredModePredictedFlag == 0:
            if MBA.MbPartPredMode() == "Intra_4x4":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx]
            elif MBA.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx >> 2]
            if MBB.MbPartPredMode() == "Intra_4x4":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx]
            elif MBB.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx >> 2]

        predIntra4x4PredMode = min(intraMxMPredModeA, intraMxMPredModeB)

        if prev_mode_flag == 1:
            return predIntra4x4PredMode
        else:
            rem_intra8x8_pred_mode = self.stream.u(3)
            if rem_intra8x8_pred_mode < predIntra4x4PredMode:
                return rem_intra8x8_pred_mode
            else:
                return rem_intra8x8_pred_mode + 1

    def decode_rem_intra8x8_pred_mode(
        self, prev_mode_flag: bool, mb_field_decoding_flag: bool, luma_blk_idx: int
    ):
        """
        Section 8.3.2.1
        """
        MBA = self.MBA
        MBB = self.MBB
        # Derive dcPredModePredictedFlag
        if (
            MBA is None
            or MBB is None
            or (
                MBA is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBA.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
            or (
                MBB is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBB.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
        ):
            dcPredModePredictedFlag = 1
        else:
            dcPredModePredictedFlag = 0

        intraMxMPredModeA = 2  # DC_MODE
        intraMxMPredModeB = 2
        if dcPredModePredictedFlag == 0:
            if MBA.MbPartPredMode() == "Intra_4x4":
                if self.mbaff == 1 and mb_field_decoding_flag == 0 and MBA.field == 1 and luma_blk_idx == 2:
                    n = 3
                else:
                    n = 1
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx * 4 + n]
            elif MBA.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx]
            if MBB.MbPartPredMode() == "Intra_4x4":
                n = 2
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx * 4 + n]
            elif MBB.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx]

        predIntra8x8PredMode = min(intraMxMPredModeA, intraMxMPredModeB)

        if prev_mode_flag == 1:
            return predIntra8x8PredMode
        else:
            rem_intra8x8_pred_mode = self.stream.u(3)
            if rem_intra8x8_pred_mode < predIntra8x8PredMode:
                return rem_intra8x8_pred_mode
            else:
                return rem_intra8x8_pred_mode + 1

    def decode_intra_chroma_pred_mode(self):
        return self.stream.ue(1)

    def decode_ref_idx(self, mbPartIdx, listIdx=0):  # keep function signature the same as cabac
        return self.stream.te(1)

    def decode_mvd(
        self, mbPartIdx, subMbPartIdx, listIdx=0, dim=0
    ):  # keep function signature the same as cabac
        return self.stream.se(1)

    def decode_CBP(self):
        CBP_intra, CBP_inter = self.stream.me(self.slice.chroma_array_type, 1)
        if self.slice.block.MbPartPredMode()[:5] == "Intra":
            return CBP_intra
        else:
            return CBP_inter

    def decode_mb_qp_delta(self):
        return self.stream.se(1)

    def last_non_zero_index(self, arr):
        for index in reversed(range(len(arr))):
            if arr[index] != 0:
                return index
        return -1

    def decode_ce(self, decode_table):
        max_bits = max(len(x) for x in decode_table.keys())
        decoded_bits = 0
        decoded_bit_count = 0
        while True:
            bit = self.stream.u(1)
            decoded_bits = (decoded_bits << 1) | bit
            decoded_bit_count += 1
            decoded_bits_str = f"{decoded_bits:0{decoded_bit_count}b}"
            if decoded_bits_str in decode_table:
                break
            if decoded_bit_count == max_bits:
                raise ValueError(
                    f"Invalid bitstream. Reached max number of bits for decode table while not finding a match. Decoded bits: {decoded_bits_str}"
                )
        return decoded_bits_str

    def decode_coeffs_token(
        self, maxNumCoeff, ctxBlockCat, residual_type, blk_idx, icbcr
    ) -> Tuple[int, int, int]:
        assert residual_type in (
            "Intra16x16DCLevel",
            "Intra16x16ACLevel",
            "LumaLevel4x4",
            "ChromaDCLevel",
            "ChromaACLevel",
            "CbIntra16x16DCLevel",
            "CbIntra16x16ACLevel",
            "CrIntra16x16DCLevel",
            "CrIntra16x16ACLevel",
            "CbLevel4x4",
            "CrLevel4x4",
        ), f"Invalid residual_type={residual_type}"

        def cond_available_flag(MBX: MacroblockParser):
            if MBX is None or (
                self.slice.block.mb_type[0] == "I"  # Intra
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBX.mb_type[0] != "I"  # Inter
                and self.slice.slice_header.nal_unit_type in (2, 3, 4)
            ):
                return 0
            else:
                return 1

        def cond_nX(MBX: MacroblockParser, blkX: np.ndarray):
            if (
                MBX is None
                or MBX.skip
                or blkX is None
                or (MBX.mb_type != "I_PCM" and ((blkX != 0).sum() == 0))
            ):
                return 0
            elif MBX.mb_type == "I_PCM":
                return 16
            else:
                return (blkX != 0).sum()

        def get_blX(MBX: MacroblockParser, block_type: str, idx: int):
            if MBX is None or MBX.skip or getattr(MBX, block_type) is None:
                return None
            else:
                if MBX.MbPartPredMode() == "Intra_16x16":
                    if block_type != "chromaACLevel":
                        return getattr(MBX, "ACLevel")[idx]
                    else:
                        return getattr(MBX, "chromaACLevel")[icbcr, idx]
                if block_type == "chromaACLevel":
                    return getattr(MBX, "chromaACLevel")[icbcr, idx]
                else:
                    return getattr(MBX, block_type)[idx]

        if residual_type == "ChromaDCLevel":
            if self.slice.chroma_array_type == 1:
                nC = -1
            else:
                nC = -2
        else:
            if residual_type in ("Intra16x16DCLevel", "CbIntra16x16DCLevel", "CrIntra16x16DCLevel"):
                blk_idx = 0

            sub_block_idx_A = 0
            sub_block_idx_B = 0
            if residual_type == "ChromaACLevel":
                MBA, sub_block_idx_A, MBB, sub_block_idx_B = self.slice.get_neighbors(4, blk_idx)
            else:
                MBA, sub_block_idx_A, MBB, sub_block_idx_B = self.slice.get_neighbors(2, blk_idx)

            if residual_type in ("Intra16x16DCLevel", "Intra16x16ACLevel", "LumaLevel4x4"):
                blkA = get_blX(MBA, "level4x4", sub_block_idx_A)
                blkB = get_blX(MBB, "level4x4", sub_block_idx_B)
            elif residual_type in ("CbIntra16x16DCLevel", "CbIntra16x16ACLevel", "CbLevel4x4"):
                blkA = get_blX(MBA, "cblevel4x4", sub_block_idx_A)
                blkB = get_blX(MBB, "cblevel4x4", sub_block_idx_B)
            elif residual_type in ("CrIntra16x16DCLevel", "CrIntra16x16ACLevel", "CrLevel4x4"):
                blkA = get_blX(MBA, "crlevel4x4", sub_block_idx_A)
                blkB = get_blX(MBB, "crlevel4x4", sub_block_idx_B)
            else:  # residual_type == "ChromaACLevel"
                blkA = get_blX(MBA, "chromaACLevel", sub_block_idx_A)
                blkB = get_blX(MBB, "chromaACLevel", sub_block_idx_B)

            availableFlagA = cond_available_flag(MBA)
            availableFlagB = cond_available_flag(MBB)

            if availableFlagA:
                nA = cond_nX(MBA, blkA)
            if availableFlagB:
                nB = cond_nX(MBB, blkB)

            if availableFlagA and availableFlagB:
                nC = (nA + nB + 1) >> 1
            elif availableFlagA and not availableFlagB:
                nC = nA
            elif not availableFlagA and availableFlagB:
                nC = nB
            else:
                nC = 0

        if 0 <= nC < 2:
            codeword_key = "0_2"
        elif 2 <= nC < 4:
            codeword_key = "2_4"
        elif 4 <= nC < 8:
            codeword_key = "4_8"
        elif 8 <= nC:
            codeword_key = "8"
        elif nC == -1:
            codeword_key = "-1"
        elif nC == -2:
            codeword_key = "-2"

        available_codewords = table_9_5[codeword_key]
        coeff_token = self.decode_ce(available_codewords)
        trailing_ones, total_coeff = available_codewords[coeff_token]

        assert not (
            maxNumCoeff == 15 and total_coeff == 16
        ), "When maxNumCoeff is equal to 15, it is a requirement of bitstream conformance that the value of TotalCoeff( coeff_token ) resulting from decoding coeff_token shall not be equal to 16"

        return trailing_ones, total_coeff, nC

    def decode_trailing_ones_sign_flag(self):
        return self.stream.u(1)

    def decode_level_prefix(self):
        """
        NOTE – The value of level_prefix is constrained to not exceed 15 in bitstreams conforming to the Baseline, Constrained Baseline, Main, and Extended profiles, as specified in clauses A.2.1, A.2.1.1, A.2.2, and A.2.3, respectively. In bitstreams conforming to other profiles, it has been reported that the value of level_prefix cannot exceed 11 + bitDepth with bitDepth being the variable BitDepthY for transform coefficient blocks related to the luma component and being the variable BitDepthC for transform coefficient blocks related to a chroma component.
        """
        decoded_bits = []
        while True:
            bit = self.stream.u(1)
            decoded_bits.append(bit)
            if bit == 1:
                break
        return len(decoded_bits) - 1

    def decode_level_suffix(self, level_prefix, suffix_length):
        if level_prefix == 14 and suffix_length == 0:
            level_suffix_size = 4
        elif level_prefix >= 15:
            level_suffix_size = level_prefix - 3
        else:
            level_suffix_size = suffix_length

        if level_suffix_size > 0:
            level_suffix = self.stream.u(level_suffix_size)
        else:
            level_suffix = 0

        return level_suffix

    def decode_run_val(self, total_coeff, maxNumCoeff):
        run_val = np.zeros(total_coeff, dtype=np.int32)
        if total_coeff == maxNumCoeff:
            zeros_left = 0
            return run_val
        else:  # total_coeff < maxNumCoeff
            # Decode total_zeros
            if maxNumCoeff == 4:
                total_zeros_decode_table = table_9_9_a
            elif maxNumCoeff == 8:
                total_zeros_decode_table = table_9_9_b
            else:
                total_zeros_decode_table = table_9_7_8  # Table 9-7 and Table 9-8

            available_codewords = total_zeros_decode_table[total_coeff]
            token = self.decode_ce(available_codewords)
            total_zeros = available_codewords[token]
            zeros_left = total_zeros

        for i in range(total_coeff - 1):
            if zeros_left > 0:
                # Decode run_before
                if zeros_left > 6:
                    available_codewords = table_9_10[7]
                else:
                    available_codewords = table_9_10[zeros_left]
                token = self.decode_ce(available_codewords)
                run_before = available_codewords[token]
                run_before = min(run_before, zeros_left)
                run_val[i] = run_before
                zeros_left -= run_before
                if zeros_left < 0:
                    raise ValueError("zeros_left < 0")
            else:
                run_val[i] = 0
                break
        run_val[-1] = zeros_left
        return run_val
