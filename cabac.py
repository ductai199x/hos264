from io import BufferedRandom
import os

import numpy as np
from bitarray import bitarray

import codes
from constants import ctx_tabs, table9_43, ctxBlockCatOffset
from bitstream import Bitstream
from slice_parser import SliceParser, MacroblockParser

relative = os.path.split(__file__)[0]
rangeTabLPS = np.loadtxt(os.path.join(relative, "rangeTab.gz"), dtype=np.uint8)
transIdxTab = np.loadtxt(os.path.join(relative, "transIdx.gz"), dtype=np.uint8)


class CabacDecoder:
    """
    WARNING: The code contained within this class is fixed. Once tested, it is
    unlikely that anything will need to change. Only make edits to this file
    if you are absolutely sure of what you're doing.
    """

    def __init__(
        self,
        file_handler: BufferedRandom,
        slice: SliceParser,
        slice_size: int,
        cabac_init=-1,
    ):
        """
        file_handler - An open file handle to the video being decoded
        slice - A Slice object containing the contex of the current frame
        slice_size - The size of the current slice in bytes
        cabac_init - The cabac initialization index parsed form P and B slice headers.
        """
        self.bitstream = bitarray()
        self.stream = Bitstream(file_handler, slice_size)
        self.ctxVars = np.zeros(1024, dtype=np.uint8)
        self.cabac_init_idc = cabac_init
        # The below attributes are needed for get_ctx
        self.slice = slice
        self.mbaff = slice.mbaff
        self.init_ctx_vars(self.slice.qpy)

    @property
    def MBA(self) -> MacroblockParser:
        return self.slice.MBA

    @property
    def MBB(self) -> MacroblockParser:
        return self.slice.MBB

    @property
    def block(self):
        return self.slice.block

    @property
    def prev(self):
        return self.slice.prev

    def read_bits(self, n):
        """
        Wrapper function around the undelying hos264.bitstream object.
        Returns n bits, and advances the bitstream.
        """
        return self.stream.u(n)

    def more_data(self):
        return self.stream.more_data()

    def decode_CBP(self):
        # maxBinIdxCtx = (3, 1)
        # ctxIdxOffset = (73, 77)
        # Decode prefix
        ctxIdx = self._get_CBPL_ctx()
        codebook = codes.FL(15)
        binstring = bitarray()
        prefix = codebook(binstring)
        while prefix is None:
            binstring.append(self.decodeBin(ctxIdx(binstring)))
            prefix = codebook(binstring)
        if self.slice.chroma_array_type in [0, 3]:
            return prefix
        # If suffix is needed too
        ctxIdx = self._get_CBPC_ctx()
        codebook = codes.TU(2)
        binstring = bitarray()
        suffix = codebook(binstring)
        while suffix is None:
            binstring.append(self.decodeBin(ctxIdx(len(binstring))))
            suffix = codebook(binstring)
        ret = prefix + (suffix << 4)
        return ret

    def _get_CBPL_ctx(self):
        """
        clause 9.3.3.1.1.4
        Coded_block_pattern_luma
        """
        ctxIdxOffset = 73

        # luma8Idx = binIdx
        def get_cond(MB, luma8, binstring):
            if MB is None or MB.mb_type == "I_PCM":
                return 0
            if MB != self.block and not MB.skip and (MB.CBPLuma >> luma8) & 1:
                return 0
            if MB == self.block and (binstring[luma8]) & 1:
                return 0
            return 1

        def get_ctxIdx(binstring):
            ibinIdx = len(binstring)
            MBA, luma8A, MBB, luma8B = self.slice.get_neighbors(5, ibinIdx)
            condA = get_cond(MBA, luma8A, binstring)
            condB = get_cond(MBB, luma8B, binstring)
            return ctxIdxOffset + condA + 2 * condB

        return get_ctxIdx

    def _get_CBPC_ctx(self):
        """
        clause 9.3.3.1.1.4
        Coded_block_pattern_chroma
        """
        MBA = self.MBA
        MBB = self.MBB
        ctxIdxOffset = 77

        def get_cond(MB, idx):
            if MB is not None and MB.mb_type == "I_PCM":
                return 1
            if MB is None or MB.skip:
                return 0
            elif idx == 0 and MB.CBPChroma == 0:
                return 0
            elif idx == 1 and not MB.CBPChroma == 2:
                return 0
            return 1

        def get_ctxIdx(ibinIdx):
            condA = get_cond(MBA, ibinIdx)
            condB = get_cond(MBB, ibinIdx)
            ctxIdxInc = condA + 2 * condB + (4 if ibinIdx == 1 else 0)
            return ctxIdxOffset + ctxIdxInc

        return get_ctxIdx

    def decode_mb_skip(self):
        ctxIdx = self._get_skip_ctx()
        skip = self.decodeBin(ctxIdx)
        return skip

    def _get_skip_ctx(self):
        """
        clause 9.3.3.1.1.1
        mb_skip_flag
        """
        MBA = self.MBA
        MBB = self.MBB
        ctxIdxOffset = 11 if "P" in self.slice.slice_type else 24
        condA = 0 if MBA is None or MBA.skip else 1
        condB = 0 if MBB is None or MBB.skip else 1
        ctxIdxInc = condA + condB
        return ctxIdxOffset + ctxIdxInc

    def decode_mb_field(self):
        ctxIdx = self._get_field_ctx()
        flag = self.decodeBin(ctxIdx)
        return flag

    def _get_field_ctx(self):
        """
        clause 9.3.3.1.1.2
        mb_field_decoding_flag
        ctxIdxOffset=70
        """
        MBA = self.MBA
        MBB = self.MBB
        ctxIdxOffset = 70
        # TODO-6: I should be checking both MBA and MBA+1
        condA = 0 if MBA is None or not MBA.field else 1
        condB = 0 if MBB is None or not MBB.field else 1
        ctxIdxInc = condA + condB
        return ctxIdxOffset + ctxIdxInc

    def decode_mb_type(self):
        stype = self.slice.slice_type
        if stype == "SI":
            return self._decode_SI_mb_type()
        elif stype == "I":
            return self._decode_I_mb_type()
        elif stype in ["P", "SP"]:
            return self._decode_P_mb_type()
        elif stype == "B":
            return self._decode_B_mb_type()

    def _get_type_ctx(self, ctxIdxOffset):
        """
        clause 9.3.3.1.1.3
        mb_type
        """
        MBA = self.MBA
        MBB = self.MBB
        condA = 1
        condB = 1
        # TODO-9: it may be expensive to return mb_type all the time
        if (
            MBA is None
            or (ctxIdxOffset == 0 and MBA.mb_type == "SI")
            or (ctxIdxOffset == 3 and MBA.mb_type == "I_NxN")
            or (ctxIdxOffset == 27 and MBA.mb_type in ["B_Skip", "B_Direct_16x16"])
        ):
            condA = 0
        if (
            MBB is None
            or (ctxIdxOffset == 0 and MBB.mb_type == "SI")
            or (ctxIdxOffset == 3 and MBB.mb_type == "I_NxN")
            or (ctxIdxOffset == 27 and MBB.mb_type in ["B_Skip", "B_Direct_16x16"])
        ):
            condB = 0
        ctxIdxInc = condA + condB
        return ctxIdxInc

    def _decode_SI_mb_type(self):
        ctxIdx = self._get_type_ctx(0)
        bit = self.decodeBin(ctxIdx)
        if bit == 0:
            return 0
        else:
            return 1 + self._decode_I_mb_type()

    def _decode_I_mb_type(self):
        ctxIdxOffset = 3
        ctxIdx = get_Ictx(self._get_type_ctx(ctxIdxOffset))
        codebook = codes.MBtype("I")
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(binstring)))
            codeword = codebook(binstring)
        if codeword == "I_PCM":
            self.init_decoder()
        return codeword

    def _decode_P_mb_type(self):
        """
        status: awaiting integration/testing
        This function is bound to be a mess, it involves 9.3.3.1.2
        """
        ctxIdxOffset = 14
        ctxIdx = get_Pctx()
        codebook = codes.MBtype("P")
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(binstring)))
            codeword = codebook(binstring)
            if binstring[0] == 1:  # If the first bit is 1, the context model must change
                ctxIdxOffset = 17
                ctxIdx = get_Ictx_in_Pslice(17)
                while codeword is None:
                    binstring.append(self.decodeBin(ctxIdx(binstring[1:])))
                    codeword = codebook(binstring)
        return codeword

    def _decode_B_mb_type(self):
        ctxIdxOffset = 27
        ctxIdx = get_Bctx(self._get_type_ctx(ctxIdxOffset))
        codebook = codes.MBtype("B")
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(binstring)))
            codeword = codebook(binstring)
            if binstring[:6].tobytes() == b"\xf4":
                ctxIdxOffset = 32
                ctxIdx = get_Ictx_in_Pslice(32)
                while codeword is None:
                    binstring.append(self.decodeBin(ctxIdx(binstring[6:])))
                    codeword = codebook(binstring)
        return codeword

    def decode_sub_mb_type(self):
        # ctxIdxOffset = 36 if self.slice_type=='B' else 21
        stype = self.slice.slice_type
        if stype == "B":  # ctxIdxOffset=36
            ctxIdx = get_subBctx()
            codebook = codes.sub_MBtype(stype)
            binstring = bitarray()
            codeword = codebook(binstring)
            while codeword is None:
                binstring.append(self.decodeBin(ctxIdx(binstring)))
                codeword = codebook(binstring)
            return codeword
        else:
            ctxIdx = get_subPctx()
            codebook = codes.sub_MBtype(stype)
            binstring = bitarray()
            codeword = codebook(binstring)
            while codeword is None:
                binstring.append(self.decodeBin(ctxIdx(binstring)))
                codeword = codebook(binstring)
            return codeword

    def decode_mvd(self, mbPartIdx, subMbPartIdx, listIdx=0, dim=0):
        # maxbinIdxCtx=4
        # ctxIdxOffset = [40,47]
        ctxIdx = self._get_mvd_ctx(mbPartIdx, subMbPartIdx, listIdx, dim=dim)
        ucoff = 9
        codebook = codes.TU(ucoff)
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(len(binstring))))
            codeword = codebook(binstring)
        if codeword < ucoff:
            if not codeword == 0 and self.decodeBin(None):
                codeword = -codeword
            return codeword
        codebook = codes.UEG3()
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(None))
            codeword = codebook(binstring)
        if self.decodeBin(None):
            codeword = -codeword
        return codeword

    def predModeEqualFlag(self, MBX, subIdx, Pred_LX):
        if MBX.mb_type in ["B_Direct_16x16", "B_Skip"]:
            return 0
        elif MBX.mb_type in ["P_8x8", "B_8x8"]:
            if MBX.subMbPredMode(subIdx) not in [Pred_LX, "BiPred"]:
                return 0
            else:
                return 1
        else:
            if MBX.MbPartPredMode(subIdx) not in [Pred_LX, "BiPred"]:
                return 0
            else:
                return 1

    def _get_mvd_ctx(self, mbPartIdx, subMbPartIdx, listIdx=0, dim=0):
        """
        clause 9.3.3.1.1.7
        Used for mvd_L{0,1}
        """
        Pred_LX = f"Pred_L{listIdx}"
        ctxIdxOffset = 40 if dim == 0 else 47
        # invoke 6.4.11.7 with mbPartIdx, sub_mb_type, subMbPartIdx
        (MBA, idxA, subIdxA), (MBB, idxB, subIdxB) = self.slice.get_neighbor_blocks(mbPartIdx, subMbPartIdx)
        compIdx = 0 if ctxIdxOffset == 40 else 1

        def absMvdComp(MBX: MacroblockParser, subIdx: int, subsubIdx: int):
            if MBX is None or MBX.skip or MBX.mb_type[0] == "I":
                return 0

            subIdx = MBX.get_blockmap()[subIdx]
            subsubIdx = MBX.get_submap(subIdx)[subsubIdx]
            mvd_lX = MBX.mvd_l0 if listIdx == 0 else MBX.mvd_l1

            if self.predModeEqualFlag(MBX, subIdx, Pred_LX) == 0:
                return 0

            if compIdx == 1 and self.mbaff and not self.block.field and MBX.field:
                return abs(mvd_lX[subIdx][subsubIdx][compIdx]) * 2
            elif compIdx == 1 and self.mbaff and self.block.field and not MBX.field:
                return abs(mvd_lX[subIdx][subsubIdx][compIdx]) // 2
            else:
                return abs(mvd_lX[subIdx][subsubIdx][compIdx])

        absMvdCompA = absMvdComp(MBA, idxA, subIdxA)
        absMvdCompB = absMvdComp(MBB, idxB, subIdxB)

        if absMvdCompA > 32 or absMvdCompB > 32:
            ctxIdxInc = 2
        elif absMvdCompA + absMvdCompB > 32:
            ctxIdxInc = 2
        elif absMvdCompA + absMvdCompB > 2:
            ctxIdxInc = 1
        else:
            ctxIdxInc = 0
        ret = [ctxIdxInc, 3, 4, 5, 6, 6, 6]

        def get_ctxIdx(binIdx):
            binIdx = min(binIdx, 6)
            return ctxIdxOffset + ret[binIdx]

        return get_ctxIdx

    def decode_ref_idx(self, mbPartIdx, listIdx=0):
        ctxIdx = self._get_refIdx_ctx(mbPartIdx, listIdx)
        codebook = codes.U()
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(len(binstring))))
            codeword = codebook(binstring)
        return codeword

    def _get_refIdx_ctx(self, mbPartIdx, listIdx=0):
        """
        clause 9.3.3.1.1.6
        used for ref_idx_l[0,1]
        """
        Pred_LX = f"Pred_L{listIdx}"
        ctxIdxOffset = 54
        # invoke 6.4.11.7 with mbPartIdx and sub_mb_type and subMbPartIdx=0
        (MBA, subIdxA), (MBB, subIdxB) = self.slice.get_neighbor_blocks(mbPartIdx)

        def cond(MBX, subIdx):
            if MBX is None or MBX.skip or MBX.mb_type[0] == "I":
                return 0

            subIdx = MBX.get_blockmap()[subIdx]

            if self.predModeEqualFlag(MBX, subIdx, Pred_LX) == 0:
                return 0

            ref_idx_lX = MBX.ref_idx_l0 if listIdx == 0 else MBX.ref_idx_l1

            if self.mbaff and not self.block.field and MBX.field:
                refIdxZeroFlagN = 0 if ref_idx_lX[subIdx] > 1 else 1
            else:
                refIdxZeroFlagN = 0 if ref_idx_lX[subIdx] > 0 else 1
            if refIdxZeroFlagN == 1:
                return 0

            return 1

        condA = cond(MBA, subIdxA)
        condB = cond(MBB, subIdxB)
        ctxIdxInc = condA + 2 * condB
        ret = [ctxIdxInc, 4, 5, 5, 5, 5, 5]

        def get_ctxIdx(binIdx):
            binIdx = min(binIdx, 2)
            return ctxIdxOffset + ret[binIdx]

        return get_ctxIdx

    def decode_mb_qp_delta(self):
        ctxIdx = self._get_qp_ctx()
        codebook = codes.mb_qp_delta()
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(len(binstring))))
            codeword = codebook(binstring)
        return codeword

    def _get_qp_ctx(self):
        """
        clause 9.3.3.1.1.5
        Used for mb_qp_delta
        """
        prev = self.prev
        ctxIdxOffset = 60
        ret0 = 1
        if (
            prev is None
            or prev.mb_type in ["P_Skip", "B_Skip", "I_PCM"]
            or (prev.CBP == 0 and not prev.MbPartPredMode() == "Intra_16x16")
            or prev.mb_qp_delta == 0
        ):
            ret0 = 0
        ret = [ret0, 2, 3, 3, 3, 3, 3]

        def get_ctxIdx(binIdx):
            binIdx = min(binIdx, 6)
            return ctxIdxOffset + ret[binIdx]

        return get_ctxIdx

    def decode_intra_chroma_pred_mode(self):
        ctxIdx = self._get_intraChroma_ctx()
        codebook = codes.TU(3)
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdx(len(binstring))))
            codeword = codebook(binstring)
        return codeword

    def _get_intraChroma_ctx(self):
        """
        clause 9.3.3.1.1.8
        used for intra_chroma_pred_mode
        """
        MBA = self.MBA
        MBB = self.MBB
        ctxIdxOffset = 64
        if (
            MBA is None
            or not MBA.mb_type[0] == "I"
            or MBA.mb_type == "I_PCM"
            or MBA.intra_chroma_pred_mode == 0
        ):
            condA = 0
        else:
            condA = 1
        if (
            MBB is None
            or not MBB.mb_type[0] == "I"
            or MBB.mb_type == "I_PCM"
            or MBB.intra_chroma_pred_mode == 0
        ):
            condB = 0
        else:
            condB = 1
        ret0 = condA + condB

        def get_ctxIdx(binIdx):
            ctxIdxInc = ret0 if binIdx == 0 else 3
            return ctxIdxOffset + ctxIdxInc

        return get_ctxIdx

    def decode_prev_mode_flag(self):
        """
        prev_intraXxX_pred_mode_flag
        """
        ctxIdxOffset = 68
        codebook = codes.FL(1)
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdxOffset))
            codeword = codebook(binstring)
        return codeword

    def get_rem_pred_mode(self):
        ctxIdxOffset = 69
        codebook = codes.FL(7)
        binstring = bitarray()
        codeword = codebook(binstring)
        while codeword is None:
            binstring.append(self.decodeBin(ctxIdxOffset))
            codeword = codebook(binstring)
        return codeword

    def decode_rem_intra4x4_pred_mode(self, prev_mode_flag: bool, luma_blk_idx: int):
        MBA = self.MBA
        MBB = self.MBB
        # Derive dcPredModePredictedFlag
        if (
            MBA is None
            or MBB is None
            or (
                MBA is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBA.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
            or (
                MBB is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBB.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
        ):
            dcPredModePredictedFlag = 1
        else:
            dcPredModePredictedFlag = 0

        intraMxMPredModeA = 2  # DC_MODE
        intraMxMPredModeB = 2
        if dcPredModePredictedFlag == 0:
            if MBA.MbPartPredMode() == "Intra_4x4":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx]
            elif MBA.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx >> 2]
            if MBB.MbPartPredMode() == "Intra_4x4":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx]
            elif MBB.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx >> 2]

        predIntra4x4PredMode = min(intraMxMPredModeA, intraMxMPredModeB)

        if prev_mode_flag == 1:
            return predIntra4x4PredMode
        else:
            rem_intra8x8_pred_mode = self.get_rem_pred_mode()
            if rem_intra8x8_pred_mode < predIntra4x4PredMode:
                return rem_intra8x8_pred_mode
            else:
                return rem_intra8x8_pred_mode + 1

    def decode_rem_intra8x8_pred_mode(
        self, prev_mode_flag: bool, mb_field_decoding_flag: bool, luma_blk_idx: int
    ):
        """
        Section 8.3.2.1
        """
        MBA = self.MBA
        MBB = self.MBB
        # Derive dcPredModePredictedFlag
        if (
            MBA is None
            or MBB is None
            or (
                MBA is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBA.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
            or (
                MBB is not None
                and self.slice.pps.constrained_intra_pred_flag == 1
                and MBB.MbPartPredMode() not in ("Intra_4x4", "Intra_8x8")
            )
        ):
            dcPredModePredictedFlag = 1
        else:
            dcPredModePredictedFlag = 0

        intraMxMPredModeA = 2  # DC_MODE
        intraMxMPredModeB = 2
        if dcPredModePredictedFlag == 0:
            if MBA.MbPartPredMode() == "Intra_4x4":
                if self.mbaff == 1 and mb_field_decoding_flag == 0 and MBA.field == 1 and luma_blk_idx == 2:
                    n = 3
                else:
                    n = 1
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx * 4 + n]
            elif MBA.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeA = MBA.sb_pred_dir[luma_blk_idx]
            if MBB.MbPartPredMode() == "Intra_4x4":
                n = 2
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx * 4 + n]
            elif MBB.MbPartPredMode() == "Intra_8x8":
                intraMxMPredModeB = MBB.sb_pred_dir[luma_blk_idx]

        predIntra8x8PredMode = min(intraMxMPredModeA, intraMxMPredModeB)

        if prev_mode_flag == 1:
            return predIntra8x8PredMode
        else:
            rem_intra8x8_pred_mode = self.get_rem_pred_mode()
            if rem_intra8x8_pred_mode < predIntra8x8PredMode:
                return rem_intra8x8_pred_mode
            else:
                return rem_intra8x8_pred_mode + 1

    def decode_EOS(self):
        ctxIdxOffset = 276
        eos = self.decodeBin(ctxIdxOffset)
        return eos

    def decode_transform8x8_flag(self):
        ctxIdx = self._get_transform8x8_ctx()
        xfrm8 = self.decodeBin(ctxIdx)
        return xfrm8

    def _get_transform8x8_ctx(self):
        """
        clause 9.3.3.1.1.10
        """
        ctxIdxOffset = 399
        MBA = self.MBA
        MBB = self.MBB
        condA = 0 if MBA is None or not MBA.transform_8x8_flag else 1
        condB = 0 if MBB is None or not MBB.transform_8x8_flag else 1
        return ctxIdxOffset + condA + condB

    def decode_coded_block_flag(self, ctxBlockCat, mbPartIdx, icbcr) -> int:
        ctxIdx = self._get_codedBlock_ctx(ctxBlockCat, mbPartIdx, icbcr)
        cbf = self.decodeBin(ctxIdx)
        return cbf

    def _get_codedBlock_ctx(self, ctxBlockCat, ndx=None, icbcr=None) -> int:
        """
        clause 9.3.3.1.1.9
        """
        block = self.slice.block
        # Step 1, determine ctxIdxOffset
        if ctxBlockCat in [5, 9, 13]:
            ctxIdxOffset = 1012
        elif ctxBlockCat < 5:
            ctxIdxOffset = 85
        elif ctxBlockCat < 9:
            ctxIdxOffset = 460
        elif ctxBlockCat < 13:
            ctxIdxOffset = 472
        # Step 2, determine ctxIdxBlockCatOffset
        ctxIdxBlockCatOffset = ctxBlockCatOffset[0][ctxBlockCat]  # table 9-40
        # Step 3, determine ctxIdxInc
        # These functions need to return either the coded_block_flag of the appropriate block, or none
        transA = 0
        transB = 0
        if ctxBlockCat in [0, 6, 10]:
            # use 6.4.11.1 to determine mbAddr[A,B]
            MBA = self.MBA
            MBB = self.MBB
            transA = transBlk_0(MBA, ctxBlockCat)
            transB = transBlk_0(MBB, ctxBlockCat)
        elif ctxBlockCat in (1, 2):
            # use 6.4.11.4 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_12(MBA, subA)
            transB = transBlk_12(MBB, subB)
        elif ctxBlockCat == 3:
            # use 6.4.11.1 to determine mbAddr[A,B]
            MBA = self.MBA
            MBB = self.MBB
            transA = transBlk_3(MBA, icbcr)
            transB = transBlk_3(MBB, icbcr)
        elif ctxBlockCat == 4:
            # use 6.4.11.5 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_4(MBA, subA, icbcr)
            transB = transBlk_4(MBB, subB, icbcr)
        elif ctxBlockCat == 5:
            # use 6.4.11.2 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_5(MBA, subA)
            transB = transBlk_5(MBB, subB)
        elif ctxBlockCat <= 8:
            # use 6.4.11.5 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_78(MBA, subA)
            transB = transBlk_78(MBB, subB)
        elif ctxBlockCat == 9:
            # use 6.4.11.3 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_9(MBA, subA)
            transB = transBlk_9(MBB, subB)
        elif ctxBlockCat <= 12:
            # use 6.4.11.5 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_1112(MBA, subA)
            transB = transBlk_1112(MBB, subB)
        elif ctxBlockCat == 13:
            # use 6.4.11.3 to determine mbAddr[A,B]
            MBA, subA, MBB, subB = self.slice.get_neighbors(ctxBlockCat, ndx)
            transA = transBlk_13(MBA, subA)
            transB = transBlk_13(MBB, subB)

        # condA and condB from transBlockN is as follows
        ###################################
        def cond(MB, trans):
            if MB is None:
                return 1 if block.MbPartPredMode()[0] == "I" else 0
            if MB.mb_type == "I_PCM":
                return 1
            if trans is None:
                return 0
            if False:  # TODO-6: compute some of these at __init__
                # if block.MbPartPredMode()[0]=='I' and self.PPS['constr_intra']==1 and MBA is not None and
                # MBA.MbPartPredMode()[0]=='I' and self.header['nal_type'] in [2,3,4]:
                return 0
            return trans

        condA = cond(MBA, transA)
        condB = cond(MBB, transB)
        ctxIdxInc = condA + 2 * condB
        # Step 4, combine to get ctxIdx
        ctxIdx = ctxIdxOffset + ctxIdxBlockCatOffset + ctxIdxInc
        return ctxIdx

    def decode_significant_coeff_map(self, ctxBlockCat, startIdx, numCoeff):
        ctxIdx, lastIdx = self._get_sigmap_ctx(ctxBlockCat, startIdx)
        coeff_map = np.zeros(numCoeff)
        coeff_map[-1] = 1
        for i in range(numCoeff - 1):
            ctx = ctxIdx(i)
            significant_coeff_flag = self.decodeBin(ctx)
            coeff_map[i] = significant_coeff_flag
            if significant_coeff_flag:
                last_significant_coeff_flag = self.decodeBin(lastIdx(i))  # ctx+lastInc
                if last_significant_coeff_flag:
                    coeff_map[-1] = 0
                    break
        return coeff_map

    def _get_sigmap_ctx(self, ctxBlockCat, startIdx):
        block = self.block
        # Step 1, determine ctxIdxOffset from table 9-34
        last_offsets = [
            [61, 61, 61, 61, 61, 15, 88, 88, 88, 30, 88, 88, 88, 30],
            [61, 61, 61, 61, 61, 15, 88, 88, 88, 24, 88, 88, 88, 24],
        ]
        if block.field:
            if ctxBlockCat < 5:
                ctxIdxOffset = 277  # 61
            elif ctxBlockCat == 5:
                ctxIdxOffset = 436  # 15
            elif ctxBlockCat < 9:
                ctxIdxOffset = 776  # 88
            elif ctxBlockCat == 9:
                ctxIdxOffset = 675  # 24
            elif ctxBlockCat < 13:
                ctxIdxOffset = 820  # 88
            elif ctxBlockCat == 13:
                ctxIdxOffset = 733  # 24
        else:
            if ctxBlockCat < 5:
                ctxIdxOffset = 105  # 61
            elif ctxBlockCat == 5:
                ctxIdxOffset = 402  # 15
            elif ctxBlockCat < 9:
                ctxIdxOffset = 484  # 88
            elif ctxBlockCat == 9:
                ctxIdxOffset = 660  # 30
            elif ctxBlockCat < 13:
                ctxIdxOffset = 528  # 88
            elif ctxBlockCat == 13:
                ctxIdxOffset = 718  # 30
        last = last_offsets[block.field][ctxBlockCat]
        # Step 2, determine ctxIdxBlockCatOffset
        ctxIdxBlockCatOffset = ctxBlockCatOffset[1][ctxBlockCat]  # table 9-40
        # Step 3, determine ctxIdxInc from 9.3.3.1.3(first segment)
        if ctxBlockCat == 3:
            ctxIdxInc = lambda ndx: min(ndx // self.slice.numC8x8, 2)
        elif ctxBlockCat in [5, 9, 13]:
            ctxIdxInc = lambda ndx: table9_43[ndx][block.field]
        else:
            ctxIdxInc = lambda ndx: ndx
        ctxIdxIncL = ctxIdxInc
        if ctxBlockCat in [5, 9, 13]:
            ctxIdxIncL = lambda ndx: table9_43[ndx][2]

        def get_ctx(idx):
            # ndx+=startIdx
            return ctxIdxOffset + ctxIdxBlockCatOffset + ctxIdxInc(idx)

        def get_Lctx(idx):
            # ndx+=startIdx
            return ctxIdxOffset + ctxIdxBlockCatOffset + ctxIdxIncL(idx) + last

        return get_ctx, get_Lctx  # last

    def decode_level_coeffs(self, ctxBlockCat, coeff_map) -> np.ndarray:
        # State variables for decoding block
        numGt = 0
        numEq = 0
        ctxIdx = self._get_coeff_ctx(ctxBlockCat)
        codebook = codes.UEGk(0, 0, 14)
        for i, coeff in reversed(list(enumerate(coeff_map))):
            if not coeff:
                continue
            binstring = bitarray()
            codeword = codebook(binstring)
            while codeword is None:
                binstring.append(self.decodeBin(ctxIdx(len(binstring), numGt, numEq)))
                codeword = codebook(binstring)
            if codeword == 0:
                numEq += 1
            else:
                numGt += 1
            coeff_map[i] += codeword
            sign = self.decodeBin(None)
            if sign:
                coeff_map[i] *= -1
        return coeff_map

    def _get_coeff_ctx(self, ctxBlockCat):
        if ctxBlockCat < 5:
            ctxIdxOffset = 227
        elif ctxBlockCat == 5:
            ctxIdxOffset = 426
        elif ctxBlockCat < 9:
            ctxIdxOffset = 952
        elif ctxBlockCat == 9:
            ctxIdxOffset = 708
        elif ctxBlockCat < 13:
            ctxIdxOffset = 982
        elif ctxBlockCat == 13:
            ctxIdxOffset = 766
        ctxIdxBlockCatOffset = ctxBlockCatOffset[3][ctxBlockCat]  # table 9-40
        cat = 3 if ctxBlockCat == 3 else 4

        def get_ctx(binIdx, numGt, numEq):
            if binIdx >= 14:
                return None
            if binIdx == 0:
                ctxIdxInc = min(4, 1 + numEq) if numGt == 0 else 0
            else:
                ctxIdxInc = 5 + min(numGt, cat)
            return ctxIdxOffset + ctxIdxBlockCatOffset + ctxIdxInc

        return get_ctx

    def init_ctx_vars(self, qpy):
        """
        Here's a little hack, we initialize all ctx, even the one for different slice types. The
        values will be garbage, but they won't matter anyway.
        """
        # self.ctxVars = np.array(1024, dtype=np.uint8)
        for i in range(1024):
            m, n = ctx_tabs[i][self.cabac_init_idc]
            if m is None:
                continue
            preCtxState = np.clip(((m * np.clip(qpy, 0, 51)) >> 4) + n, 1, 126)
            if preCtxState <= 63:
                self.ctxVars[i] = (63 - preCtxState) << 1
            else:
                self.ctxVars[i] = ((preCtxState - 64) << 1) | 1
        self.init_decoder()

    def init_decoder(self):
        self.codIRange = 510
        off = self.read_bits(9)
        self.codIOffset = np.uint16(off)

    def decodeBin(self, ctxIdx=None, bypass=False) -> int:
        bypass = ctxIdx is None
        # if ctxIdx is None: bypass=True
        # DecodeBypass - 9.3.3.2.3
        if bypass:
            self.codIOffset <<= 1
            self.codIOffset |= self.read_bits(1)
            if self.codIOffset < self.codIRange:
                return 0
            self.codIOffset -= self.codIRange
            return 1
        # DecodeTerminate - 9.3.3.2.4
        if ctxIdx == 276:
            self.codIRange -= 2
            if self.codIOffset >= self.codIRange:
                return 1
            self.renorm()
            return 0
        # DecodeDecision - 9.3.3.2.1
        # print(f"offset is {self.codIOffset}")
        MPS = self.ctxVars[ctxIdx] & 1
        pState = self.ctxVars[ctxIdx] >> 1
        # print(f"state is {self.ctxVars[ctxIdx]} range is {self.codIRange}")
        # print(f"offset is {self.codIOffset}, at {self.stream.tell()}")
        qCodIRangeIdx = (self.codIRange >> 6) & 3
        rangeLPS = rangeTabLPS[pState, qCodIRangeIdx]
        self.codIRange -= rangeLPS
        if self.codIOffset < self.codIRange:
            retval = MPS
            self.ctxVars[ctxIdx] = (transIdxTab[pState, 1] << 1) | MPS
        else:
            retval = not MPS
            self.codIOffset -= self.codIRange
            self.codIRange = rangeLPS
            if pState == 0:
                MPS = 1 - MPS
            self.ctxVars[ctxIdx] = (transIdxTab[pState, 0] << 1) | MPS
        self.renorm()
        # print(f"range after is {self.codIRange}")
        # print(f"offset is {self.codIOffset}")
        return retval

    def renorm(self):
        """
        9.3.3.2.2
        """
        while self.codIRange < 256:
            self.codIRange <<= 1
            if self.codIRange == 0:
                raise ValueError("How did a 0 get here?")
            self.codIOffset <<= 1
            self.codIOffset |= self.read_bits(1)


# clause 9.3.3.1.1.9
################################################################
def transBlk_0(MB, ctxBlockCat):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or not MB.MbPartPredMode() == "Intra_16x16":
        return None
    if ctxBlockCat == 0:
        return MB.CBF["y"]["DC"]
    if ctxBlockCat == 6:
        return MB.CBF["cbcr"][0]["DC"]
    if ctxBlockCat == 10:
        return MB.CBF["cbcr"][1]["DC"]


def transBlk_12(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> (subIdx >> 2)) & 1) == 0:
        return None
    if MB.transform_8x8_flag:
        key = "level8"
        subIdx >>= 2
    else:
        key = "AC" if MB.MbPartPredMode() == "Intra_16x16" else "level4"
    transA = MB.CBF["y"][key][subIdx]
    return transA


def transBlk_3(MB, icbcr):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or MB.CBPChroma == 0:
        return None
    return MB.CBF["cbcr"][icbcr]["DC"]


def transBlk_4(MB, subIdx, icbcr):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or not MB.CBPChroma == 2:
        return None
    return MB.CBF["cbcr"][icbcr]["AC"][subIdx]


def transBlk_5(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> subIdx) & 1) == 0:
        return None
    if not MB.transform_8x8_flag:
        return None
    return MB.CBF["y"]["level8"][subIdx]


def transBlk_78(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> (subIdx >> 2)) & 1) == 0:
        return None
    if MB.transform_8x8_flag:
        key = "level8"
        subIdx <<= 2
    else:
        key = "AC" if MB.MbPartPredMode() == "Intra_16x16" else "level4"
    transA = MB.CBF["cbcr"][0][key][subIdx]
    return transA


def transBlk_9(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> subIdx) & 1) == 0:
        return None
    if not MB.transform_8x8_flag:
        return None
    return MB.CBF["cbcr"][0]["level8"][subIdx]


def transBlk_1112(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> (subIdx >> 2)) & 1) == 0:
        return None
    if MB.transform_8x8_flag:
        key = "level8"
        subIdx <<= 2
    else:
        key = "AC" if MB.MbPartPredMode() == "Intra_16x16" else "level4"
    transA = MB.CBF["cbcr"][1][key][subIdx]
    return transA


def transBlk_13(MB, subIdx):
    """
    Part of 9.3.3.1.1.9
    """
    if MB is None or MB.mb_type in ["P_Skip", "B_Skip", "I_PCM"] or ((MB.CBPLuma >> subIdx) & 1) == 0:
        return None
    if not MB.transform_8x8_flag:
        return None
    return MB.CBF["cbcr"][1]["level8"][subIdx]


################################################################


# clause 9.3.3.1.2
################################################################
def get_Ictx(ctx0):
    """
    Part of 9.3.3.1.2
    """
    ret = [ctx0, 276 - 3, 3, 4, 5, 6, 7]

    def get_ctxIdx(query):
        inc = 0
        if len(query) in [4, 5] and query[3] == 0:
            inc = 1
        return 3 + ret[len(query)] + inc

    return get_ctxIdx


def get_Pctx():
    """
    Part of 9.3.3.1.2
    """

    def get_ctxIdx(query):
        inc = 0
        if len(query) == 2 and query[1] == 1:
            inc = 1
        return 14 + len(query) + inc

    return get_ctxIdx


def get_Bctx(ctx0):
    """
    Part of 9.3.3.1.2
    """
    ret = [ctx0, 3, 4, 5]

    def get_ctxIdx(query):
        inc = 0
        if len(query) == 2 and query[1] == 0:
            inc = 1
        return 27 + ret[min(len(query), 3)] + inc

    return get_ctxIdx


def get_Ictx_in_Pslice(ctxIdxOffset):
    """
    Part of 9.3.3.1.2
    """
    ret = [0, 276 - ctxIdxOffset, 1, 2, 2, 3]

    def get_ctxIdx(query):
        inc = 0
        leng = min(len(query), 5)
        if leng == 4 and query[3] == 0:
            inc = 1
        return ret[leng] + ctxIdxOffset + inc

    return get_ctxIdx


def get_subPctx():
    """
    Part of 9.3.3.1.2
    """

    def get_ctxIdx(query):
        return 21 + len(query)

    return get_ctxIdx


def get_subBctx():
    """
    Part of 9.3.3.1.2
    """
    ret = [0, 1, 2, 3]

    def get_ctxIdx(query):
        inc = 0
        if len(query) == 2 and query[1] == 0:
            inc = 1
        return 36 + ret[min(len(query), 3)] + inc

    return get_ctxIdx


################################################################
