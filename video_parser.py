import os
from typing import *

import numpy as np
from bitarray import bitarray

from bitstream import Bitstream
from constants import *
from frame_parser import FrameParser
from pps_dataclass import PPS
from slice_header_dataclass import SliceHeader
from slice_parser import SliceParser
from sps_dataclass import SPS

# Some helpful constants and lookup tables
SLICE_TYPES = "PBIpiPBIpi"
path_to_stbl = [b"moov", b"trak", b"mdia", b"minf", b"stbl"]


def b2i(bstring, signed=False):
    """
    Wrapper with defaults around int.from_bytes

    bstring - the bytestring to be cast
    signed - if true, the bytestring should be read as a signed integer.
    """
    return int.from_bytes(bstring, byteorder="big", signed=signed)


def i2b(num, num_b=4):
    """
    Wrapper with defaults around int.to_bytes()

    num - the integer to be cast
    num_b - the number of bytes to be returned
    """
    return num.to_bytes(num_b, byteorder="big")


def read_box_header(f):
    size = b2i(f.read(4))
    box_type = f.read(4).decode("ascii")
    return size, box_type


class VideoParser:
    """
    A VideoParser object to interact with an mp4 file, specifically the sample table, and slice headers of H.264 encoded slices..

    The standards covered by this code are numerous, lengthy, and nuanced. As such, it is very possible that some functionality will fail when used with abnormal or non-standard video. In the event of odd failures, please raise an issue or email Brian.C.Hosler@gmail.com.
    """

    def __init__(self, fpath, prescan=True):
        """
        Initialize a video object

        fpath - a path to an ISO/IEC:14496-12 compliant mp4 file containing an H.264 encoded video
        prescan - if true, the sample table will be read upon creation. This is a negligable
        processing cost, and should almost always be performed upon creation.

        The general steps to decode macroblock information from an H.264 bitstream are as follows:
        1. Extract the NAL (Network Abstraction Layer) units: H.264 uses NAL units to encapsulate different parts of the video data, such as frame headers and macroblock data. You'll need to extract these NAL units from the bitstream by searching for start codes (0x000001 or 0x00000001) and parsing the NAL unit type.

        2. Parse the SPS (Sequence Parameter Set) and PPS (Picture Parameter Set): SPS and PPS provide information about the video sequence and individual pictures, such as frame size, macroblock size, and other parameters. You'll need this information to interpret the macroblock data correctly.

        3. Parse the slice headers: Each frame consists of one or more slices, and each slice starts with a slice header. The slice header contains information about the type of slice, such as I, P, or B slice, and the macroblock address.

        4. Decode the macroblock data: After parsing the slice headers, you can proceed to decode the macroblock data. Macroblock data consists of a series of macroblock layer elements, such as macroblock type, motion vectors, intra/inter prediction modes, and transform coefficients for residual data. The syntax and semantics of these elements depend on the video profile, level, and encoding settings.
        """
        self.fname = fpath
        # self.fhdl = None
        self.open()
        if not self.ismp4():
            print(f"ERROR\nFile {fpath} does not appear to be an mp4 file.\n")
            raise NotImplementedError
        self.SPS: Dict[int, SPS] = {}
        self.PPS: Dict[int, PPS] = {}
        self.slice_headers: Dict[int, SliceHeader] = {}
        self.width = 0
        self.height = 0
        self.stbl = None
        self.pts = None
        if prescan:
            self.build_stbl()

    def __repr__(self):
        """ """
        return f"hos264.video({os.path.basename(self.fname)}, {len(self.stbl)} Frames)"

    def build_stbl(self):
        """
        Attempts to find, read and translate the video sample table, in order to determine the
        location and display time of frames/slices.

        To seek to the video sample table in an MP4 file, you need to parse the file's structure, which is organized into a hierarchy of boxes (also called atoms). The video sample table is stored within the 'stbl' box, which is nested inside other boxes. Here's a high-level overview of the MP4 box structure you need to traverse:

        1. 'ftyp': File Type Box
        2. 'moov': Movie Box
            - 'trak': Track Box
                - 'mdia': Media Box
                    - 'minf': Media Information Box
                        - 'stbl': Sample Table Box
                            - 'stsd': Sample Description Box
                            - 'stsc': Sample-to-Chunk Box
                            - 'stco' or 'co64': Chunk Offset Box
                            - 'stts': Time-to-Sample Box
                            - 'ctts': Composition Time-to-Sample Box
                            - 'stsz': Sample Size Box
                            - 'stss': Sync Sample Box

        To parse the MP4 file and seek to the video sample table, follow these steps:

        1. Read the 'ftyp' box: Start by reading the 'ftyp' box, which contains information about the file format and compatibility.

        2. Locate the 'moov' box: Search for the 'moov' box, which contains metadata for the entire file.

        3. Find the 'trak' box for the video track: Inside the 'moov' box, locate the 'trak' box corresponding to the video track. You may need to check the 'hdlr' (handler) box within the 'mdia' (media) box to determine if the track is a video track.

        4. Traverse to the 'stbl' box: Inside the video track's 'trak' box, navigate to the 'stbl' box by going through 'mdia' -> 'minf' -> 'stbl'.

        5. Access the sample table boxes: Within the 'stbl' box, you'll find the boxes containing the video sample table, such as 'stsd', 'stts', 'stsc', 'stsz', and 'stco' or 'co64'.
        """

        def find_box(f, target_box_type):
            while True:
                size, box_type = read_box_header(f)
                if box_type == target_box_type:
                    return size
                f.seek(size - 8, 1)  # Skip the current box

        def is_video_trak(f):
            current_position = f.tell()
            find_box(f, "mdia")
            find_box(f, "hdlr")
            f.read(8)  # Skip version, flags and pre_defined fields
            handler_type = f.read(4).decode("ascii")
            f.seek(current_position)  # Return to the beginning of the "trak" box
            return handler_type == "vide"

        f = self.fhdl
        f.seek(0)

        find_box(f, "moov")
        while True:  # There maybe multiple "trak" boxes, we need to find the one for the video track
            trak_size = find_box(f, "trak")
            trak_start = f.tell() - 8
            if is_video_trak(f):
                break
            f.seek(trak_size - 8, 1)  # Skip the current "trak" box if it"s not a video track
        find_box(f, "mdia")
        find_box(f, "minf")
        stbl_size = find_box(f, "stbl")
        stbl_start = f.tell()
        stbl_end = stbl_start + stbl_size

        stsd, stsc, stco, stts, ctts, stsz, stss = None, None, None, None, None, None, None
        while f.tell() < stbl_end - 1:
            buff = f.read(16)
            size = b2i(buff[:4])
            name = buff[4:8]
            num = b2i(buff[8:])
            if name == b"stsd":
                stsd = self._consume_stsd(num)  # Sample Description Box
            elif name == b"stsc":
                stsc = self._consume_stsc(num)  # Sample-to-Chunk Box
            elif name == b"stco":
                stco = self._consume_stco(num)  # Chunk Offset Box
            elif name == b"co64":
                stco = self._consume_co64(num)  # 64-bit Chunk Offset Box
            elif name == b"stts":
                stts = self._consume_stts(num)  # Time-to-Sample Box
            elif name == b"ctts":
                ctts = self._consume_ctts(num)  # Composition Time-to-Sample Box
            elif name == b"stsz":
                stsz = self._consume_stsz(num)  # Sample Size Box
            elif name == b"stss":
                stss = self._consume_stss(num)  # Sync Sample Box
            else:
                f.seek(size - 16, 1)  # skip the rest of the box

        # Now that we have all the tables, lets build the sample table!

        # The stsc table contains records that indicate the number of samples per chunk in the video track. Each entry in stsc has two values: fst (first_chunk) and n (samples_per_chunk). This loop iterates over the stsc entries and assigns the number of samples per chunk (n) to the corresponding chunk in the stco (chunk offset) table. The stco table has multiple rows, where each row contains two values: chunk offset and the number of samples per chunk. The expression fst - 1 is used to convert the 1-based index of the first chunk to a 0-based index used in Python.
        for fst, n in stsc:
            stco[fst - 1 :, 1] = n
        # The stts table contains information about the duration of each sample in the video track. The ctts table, if present, contains information about the composition time offset of each sample in the video track. The composition time offset is used to adjust the presentation time of each sample, which is necessary when samples are not presented in the order they are stored in the file (for example, in the case of B-frames). The pts table contains the presentation time of each sample in the video track. The pts table is calculated by adding the duration of each sample to the composition time offset of the sample. The pts table is used to determine the order in which samples are displayed.
        if ctts is not None:
            pts = [(s + c) for s, c in zip(stts, ctts)]
        else:
            pts = stts

        # This piece of code constructs the sample table (stbl) containing the start offsets of each sample in the video track. It uses the stco (chunk offset) and stsz (sample size) tables to achieve this. Each row (chunk) in the stco table contains 2 values: the file offset for the start of the chunk and the number of samples in the chunk.
        stbl = []
        curr_samp = 0
        for row in stco:
            f.seek(row[0])  # seek to the start of the chunk
            for i in range(row[1]):
                start_samp = f.tell()
                end_samp = start_samp + stsz[curr_samp]
                stbl.append(start_samp)
                curr_samp += 1
                f.seek(end_samp)
        self.stbl = np.array(stbl)
        self.stsz = np.array(stsz)
        self.pts = pts
        self.stss = stss

    def get_frame_properties(self, display=True) -> Dict[str, Union[int, str]]:
        """
        Returns the frame properties of each frame in the H.264/AVC bitstream.

        Args:
            display (bool): If True, the frame properties are returned in display order. If False (default),
                the frame properties are returned in decode order.

        Returns:
            A dictionary containing the frame properties of each frame in the bitstream. The three properties returned are: "frame_type" (the frame type, which can be "I" (intra-coded frame), "P" (predictive-coded frame), and "B" (bi-predictive-coded frame)), "frame_size" (the size of the frame in bytes), and "quant_param" (the quantization parameter used to encode the frame).
        """

        f = self.fhdl
        frame_types = []
        frame_sizes = []
        frame_quant_params = []
        slice_headers_ = []
        # Loop over all samples in the sample table
        for sample_start, sample_size in zip(self.stbl, self.stsz):
            f.seek(sample_start)
            sample_end = f.tell() + sample_size
            # Loop over all slices in the sample
            while f.tell() < sample_end:
                buff = f.read(5)
                # Parse the slice header
                slice_size = b2i(buff[: self.nal_length_size])
                end = f.tell() + slice_size - 1
                nal_unit_type = buff[4] & 31
                if nal_unit_type in [1, 5]:
                    # Parse the slice header
                    slice_header = self.slice_header_parser(slice_size)
                    slice_headers_.append(slice_header)
                    # Get the quantization parameter
                    frame_quant_params.append(slice_header.slice_qp_delta)
                    # Get the frame type
                    frame_types.append(slice_header.slice_type)
                    # Get the frame size
                    frame_sizes.append(slice_size)
                f.seek(end)
        if display:
            _, slice_headers_ = zip(*sorted(zip(self.pts, slice_headers_)))
            _, frame_types = zip(*sorted(zip(self.pts, frame_types)))
            _, frame_sizes = zip(*sorted(zip(self.pts, frame_sizes)))
            _, frame_quant_params = zip(*sorted(zip(self.pts, frame_quant_params)))

        self.slice_headers = slice_headers_

        return {
            "frame_types": frame_types,
            "frame_sizes": frame_sizes,
            "frame_quant_params": frame_quant_params,
        }

    def seek_to_sample(self, num):
        if num > len(self.stbl) or num < 0:
            raise IndexError(f"Index {num} not in range (0,{len(self.stbl)})")
        pts, stbl, stsz = zip(*sorted(zip(self.pts, self.stbl, self.stsz)))
        off = stbl[num]
        self.fhdl.seek(off)
        return stsz[num]

    def get_frame(self, idx=0) -> SliceParser:
        f = self.fhdl
        slice_header = self.slice_headers[idx]
        f.seek(0)
        f.seek(slice_header.slice_end_file_position[0])

        print(f"Parsing Frame: Idx={idx}, Type={slice_header.slice_type}, Coding Method={'CABAC' if self.PPS[slice_header.pic_parameter_set_id].entropy_coding_mode_flag else 'CAVLC'}, Size={slice_header.slice_size}B, @{self.fhdl.tell()}")
        
        frame_parser = FrameParser(self, slice_header.slice_size, slice_header)
        frame = frame_parser.get_macroblocks()
        return frame

    def frames(self) -> Generator[SliceParser, None, None]:
        for i in range(len(self.stbl)):
            f = self.get_frame(i, True)
            yield f

    def reblock(self, num):
        if isinstance(num, int):
            num = [num]
        f = self.fhdl
        for idx in num:
            slice_header = self.slice_headers[idx]
            tell, off = slice_header.deblocking_file_pointer_position
            f.seek(tell)
            # Flipping the bit to turn ON the deblocking filter
            stream = Bitstream(f, 2)
            stream.read(2)
            bits = stream.stream[off : off + 3]
            if not bits == bitarray("111"):
                raise ValueError(f"Bits {bits} are not equal to 111")
            stream.stream[off : off + 3] = bitarray("010")
            towrite = stream.stream.tobytes()
            f.seek(tell)
            f.write(towrite)
        f.flush()

    def deblock(self, num):
        if isinstance(num, int):
            num = [num]
        f = self.fhdl
        for idx in num:
            slice_header = self.slice_headers[idx]
            tell, off = slice_header.deblocking_file_pointer_position
            f.seek(tell)
            # Flipping the bit to turn ON the deblocking filter
            stream = Bitstream(f, 2)
            stream.read(2)
            bits = stream.stream[off : off + 3]
            if not bits == bitarray("010"):
                raise ValueError(f"Bits {bits} are not equal to 010")
            stream.stream[off : off + 3] = bitarray("111")
            towrite = stream.stream.tobytes()
            f.seek(tell)
            f.write(towrite)

    def _consume_stsd(self, num):
        """
        Reads and interprets
        Reads and interprates the sample description table
        """
        f = self.fhdl
        for i in range(num):
            start = f.tell()
            buff = f.read(4 + 4 + 8 + 2)
            size = b2i(buff[:4])
            fmt = buff[4:8]
            # TODO: check size of reserved, 6 or 8 bytes?
            # reserved = buff[8:16]
            dref = b2i(buff[-2:])
            if fmt != b"avc1":
                raise NotImplementedError(f"Format {fmt} not supported")
            version = f.read(2)
            revision = f.read(2)
            vendor = f.read(4)
            temp_comp = b2i(f.read(4))
            spac_comp = b2i(f.read(4))
            self.width = b2i(f.read(2))
            self.height = b2i(f.read(2))
            h_res = b2i(f.read(4))
            v_res = b2i(f.read(4))
            dat_size = b2i(f.read(4))
            fram_cnt = b2i(f.read(2))
            name_buff = f.read(32)
            comp_name_size = name_buff[0]
            comp_name = name_buff[1 : 1 + comp_name_size]
            bit_depth = f.read(2)
            color_tab_ID = b2i(f.read(2))
            if not (color_tab_ID == 0 or color_tab_ID == -1):
                ctab_buff = f.read(8)
                ctab_size = b2i(ctab_buff[:4])
                f.seek(ctab_size - 8, 1)
            self._consume_avcc()
            f.seek(start + size)

    def _consume_avcc(self):
        """
        Reads and interprets
        reads and interperates the AVCc box within a sample description table
        """
        f = self.fhdl
        buff = f.read(6)
        size = b2i(buff[:2])
        name = buff[2:]
        if not name == b"avcC":
            # print(f"{name}")
            f.seek(size - 6, 1)
            return
        buff = f.read(4)
        # version = b2i(buff[0])
        self.profile = buff[1]
        # compat = b2i(buff[2])
        self.level = buff[3]
        self.nal_length_size = (
            b2i(f.read(1)) & 3 + 1
        )  # the 'lengthSizeMinusOne' field value in the 'avcC' box inside the 'avc1' box
        num_sps = b2i(f.read(1)) & 0x1F  # Use 0x1F (31) to extract the least significant 5 bits
        for i in range(num_sps):
            size = b2i(f.read(2))  # nal_len_bytes))
            end_nal = f.tell() + size
            naltype = b2i(f.read(1)) & 31
            if naltype == 7:
                self.sps_parser(size)
            f.seek(end_nal)
        num_pps = b2i(f.read(1)) & 0xFF  # Use 0xFF (255) to extract the full 8-bit unsigned integer
        for i in range(num_pps):
            size = b2i(f.read(2))  # nal_len_bytes))
            end_nal = f.tell() + size
            naltype = b2i(f.read(1)) & 31
            if naltype == 8:
                self.pps_parser(size)
            f.seek(end_nal)

    def _consume_stsc(self, num):
        """
        Reads and interprets the sampleto chunk table.

        num - tne number of table entries
        """
        f = self.fhdl
        stsc = []
        for i in range(num):
            buff = f.read(12)
            fst = b2i(buff[:4])
            n = b2i(buff[4:8])
            stsc.append((fst, n))
        stsc.sort()
        return stsc

    def _consume_stco(self, num):
        """
        Reads and interprets the chunk offset table.

        num - tne number of table entries
        """
        f = self.fhdl
        stco = np.zeros((num, 2), dtype=np.uint32)
        for i in range(num):
            buff = f.read(4)
            stco[i, 0] = b2i(buff)
        return stco

    def _consume_co64(self, num):
        """
        Reads and interprets the 64-bit chunk offset table.
        Note: this is one of those oddities that have caused this code to fail previously.

        num - tne number of table entries
        """
        f = self.fhdl
        stco = np.zeros((num, 2), dtype=np.uint32)
        for i in range(num):
            buff = f.read(8)
            stco[i, 0] = b2i(buff)
        return stco

    def _consume_stts(self, num):
        """
        Reads and interprets the time to sample table.

        num - tne number of table entries
        """
        # Composition time is ctts[n]+sum(stts[:n]), so it must start with 0
        f = self.fhdl
        stts = [0]  # I need to have a stts[-1] that is not none
        for i in range(num):
            buff = f.read(8)
            n = b2i(buff[:4])
            durtn = b2i(buff[4:])
            for k in range(n):
                stts.append(stts[-1] + durtn)
        # stts = stts[1:]
        return stts

    def _consume_ctts(self, num):
        """
        Reads and interprets the composition offset table.

        num - tne number of table entries
        """
        f = self.fhdl
        ctts = [0]  # Just to match stts, I guess. could start with []
        for i in range(num):
            buff = f.read(8)
            n = b2i(buff[:4])
            off = b2i(buff[4:], signed=True)
            for k in range(n):
                ctts.append(off)
        ctts = ctts[1:]  # remove this line if starting with [], not [0]
        return ctts

    def _consume_stsz(self, siz):
        """
        Reads and interprets the sample size table.
        Note: This table may be unreliable for creating an EFS sequence because of fake or non-slice
        data NAL units like SEI or SPS/PPS. Because of this, it is not used for creating EFS
        sequences.

        siz - the size of every sample. If 0 indicates that sample are different sizes(Basically
        always).
        """
        f = self.fhdl
        num = b2i(f.read(4))
        if not siz == 0:
            return siz
        stsz = []
        for i in range(num):
            stsz.append(b2i(f.read(4)))
        return stsz

    def _consume_stss(self, num):
        """
        Reads and interprets the sync sample table.

        num - tne number of table entries
        """
        f = self.fhdl
        stss = np.zeros(num, dtype=np.uint32)
        for i in range(num):
            buff = f.read(4)
            stss[i] = b2i(buff)
        return stss

    def slice_header_parser(self, slice_size: int) -> SliceHeader:
        """
        Verified this with H.264 Standard 08/2021. Page 51, Section 7.3.3 Slice header syntax.
        This function parses the slice header and returns a dictionary of the fields.

        Args:
            slice_size (int): The size of the slice in bytes.

        Returns:
            SliceHeader: A SliceHeader dataclass containing the slice header's fields.
        """
        """
        I'm writing a H.264 video parser in Python. I'm at the part where I need to extract *all* the fields in the slice header. Currently, my function have 2 input arguments: f and slice_size. f is the file handling object and slice_size is the current slice's size in bytes. Additionally, I also have a bitstream class in which I can use to decode the bits. Assume stream is a valid bitstream object, stream.u(n) will read n bits and convert them to unsigned int, stream.ue(n) will decode n numel syntax elements using unsigned expgolomb coding, and stream.se(n) will decode n numel syntax elements using signed expgolomb coding. Can you help me write this function? Please only give me the code to extract one group of related fields at a time, then stop. Only continue the code for the next group of fields when I say "Continue". Understand?
        """
        f = self.fhdl
        f.seek(-1, 1)  # back up to the position of the nal_unit_type byte
        slice_start_file_position = f.tell()
        stream = Bitstream(f, slice_size)

        nal_ref_idc = stream.u(3)  # forbidden + 2
        nal_unit_type = stream.u(5)

        # first_mb_in_slice (ue-v)
        first_mb_in_slice = stream.ue(1)

        # slice_type (ue-v)
        slice_type = stream.ue(1)
        slice_type_str = SLICE_TYPES[slice_type]
        if slice_type_str == "i":
            slice_type_str = "SI"
        if slice_type_str == "p":
            slice_type_str = "SP"

        # pic_parameter_set_id (ue-v)
        pic_parameter_set_id = stream.ue(1)

        pps = self.PPS[pic_parameter_set_id]
        sps = self.SPS[pps.seq_parameter_set_id]

        # colour_plane_id (u-2, if applicable)
        if sps.separate_colour_plane_flag:
            colour_plane_id = stream.u(2)
        else:
            colour_plane_id = None

        # frame_num (u-v)
        frame_num = stream.u(sps.log2_max_frame_num_minus4 + 4)

        # field_pic_flag (u-1)
        if not sps.frame_mbs_only_flag:
            field_pic_flag = stream.u(1)
            if field_pic_flag:
                bottom_field_flag = stream.u(1)
            else:
                bottom_field_flag = 0
        else:
            field_pic_flag = 0
            bottom_field_flag = 0

        # idr_pic_id (ue-v, if applicable)
        if slice_type_str in ("I", "SI"):
            idr_pic_id = stream.ue(1)
        else:
            idr_pic_id = None

        # pic_order_cnt_lsb (u-v, if applicable)
        if sps.pic_order_cnt_type == 0:
            pic_order_cnt_lsb = stream.u(sps.log2_max_pic_order_cnt_lsb_minus4 + 4)
            if pps.bottom_field_pic_order_in_frame_present_flag and not field_pic_flag:
                delta_pic_order_cnt_bottom = stream.se(1)
            else:
                delta_pic_order_cnt_bottom = None
        else:
            pic_order_cnt_lsb = None
            delta_pic_order_cnt_bottom = None

        # delta_pic_order_cnt_0 or _1 (se-v, if applicable)
        if sps.pic_order_cnt_type == 1 and not sps.delta_pic_order_always_zero_flag:
            delta_pic_order_cnt_0 = stream.se(1)
            if pps.bottom_field_pic_order_in_frame_present_flag and not field_pic_flag:
                delta_pic_order_cnt_1 = stream.se(1)
            else:
                delta_pic_order_cnt_1 = None
        else:
            delta_pic_order_cnt_0 = None
            delta_pic_order_cnt_1 = None

        # redundant_pic_cnt_present_flag (from PPS)
        redundant_pic_cnt_present_flag = pps.redundant_pic_cnt_present_flag

        # redundant_pic_cnt (ue-v, if applicable)
        if redundant_pic_cnt_present_flag:
            redundant_pic_cnt = stream.ue(1)
        else:
            redundant_pic_cnt = None

        # direct_spatial_mv_pred_flag (u(1), if applicable)
        if slice_type_str == "B":
            direct_spatial_mv_pred_flag = stream.u(1)
        else:
            direct_spatial_mv_pred_flag = None

        # num_ref_idx_active_override_flag (u(1), if applicable)
        if slice_type_str in ("P", "SP", "B"):
            num_ref_idx_active_override_flag = stream.u(1)
        else:
            num_ref_idx_active_override_flag = 0

        # num_ref_idx_l0_active_minus1 (ue-v, if applicable)
        if num_ref_idx_active_override_flag:
            num_ref_idx_l0_active_minus1 = stream.ue(1)
        else:
            num_ref_idx_l0_active_minus1 = pps.num_ref_idx_l0_default_active_minus1

        # num_ref_idx_l1_active_minus1 (ue-v, if applicable)
        if slice_type_str == "B" and num_ref_idx_active_override_flag:
            num_ref_idx_l1_active_minus1 = stream.ue(1)
        else:
            num_ref_idx_l1_active_minus1 = pps.num_ref_idx_l1_default_active_minus1

        # ref_pic_list_reordering (if applicable)
        ref_pic_list_reordering = []
        if not nal_unit_type in (20, 21):
            if slice_type % 5 not in (2, 4):  # Not I or SI slice
                ref_pic_list_reordering_flag_l0 = stream.u(1)
                if ref_pic_list_reordering_flag_l0:
                    reordering_of_pic_nums_idc = -1
                    while reordering_of_pic_nums_idc != 3:
                        reordering_of_pic_nums_idc = stream.ue(1)
                        if reordering_of_pic_nums_idc in (0, 1):
                            abs_diff_pic_num_minus1 = stream.ue(1)
                        elif reordering_of_pic_nums_idc == 2:
                            long_term_pic_num = stream.ue(1)
                        ref_pic_list_reordering.append(reordering_of_pic_nums_idc)

            if slice_type % 5 == 1:  # B slice
                ref_pic_list_reordering_flag_l1 = stream.u(1)
                if ref_pic_list_reordering_flag_l1:
                    reordering_of_pic_nums_idc = -1
                    while reordering_of_pic_nums_idc != 3:
                        reordering_of_pic_nums_idc = stream.ue(1)
                        if reordering_of_pic_nums_idc in (0, 1):
                            abs_diff_pic_num_minus1 = stream.ue(1)
                        elif reordering_of_pic_nums_idc == 2:
                            long_term_pic_num = stream.ue(1)
                        ref_pic_list_reordering.append(reordering_of_pic_nums_idc)
        else:  # This is for MVC, not AVC
            pass

        # pred_weight_table (if applicable)
        luma_log2_weight_denom = 5
        chroma_log2_weight_denom = 5
        luma_weight_l0 = 2**luma_log2_weight_denom
        luma_offset_l0 = 0
        chroma_weight_l0 = 2**chroma_log2_weight_denom
        chroma_offset_l0 = 0
        luma_weight_l1 = 2**luma_log2_weight_denom
        luma_offset_l1 = 0
        chroma_weight_l1 = 2**chroma_log2_weight_denom
        chroma_offset_l1 = 0

        if (pps.weighted_pred_flag and slice_type_str in ("P", "SP")) or (
            pps.weighted_bipred_idc == 1 and slice_type_str == "B"
        ):
            luma_log2_weight_denom = stream.ue(1)
            chroma_log2_weight_denom = stream.ue(1) if sps.chroma_format_idc != 0 else None

            luma_weight_l0 = []
            luma_offset_l0 = []
            chroma_weight_l0 = []
            chroma_offset_l0 = []
            for i in range(num_ref_idx_l0_active_minus1 + 1):
                luma_weight_l0_flag = stream.u(1)
                if luma_weight_l0_flag:
                    luma_weight_l0.append(stream.se(1))
                    luma_offset_l0.append(stream.se(1))
                else:
                    luma_weight_l0.append(None)
                    luma_offset_l0.append(None)

                if sps.chroma_format_idc != 0:
                    chroma_weight_l0_flag = stream.u(1)
                    if chroma_weight_l0_flag:
                        chroma_weight_l0_i = []
                        chroma_offset_l0_i = []
                        for j in range(2):
                            chroma_weight_l0_i.append(stream.se(1))
                            chroma_offset_l0_i.append(stream.se(1))
                        chroma_weight_l0.append(chroma_weight_l0_i)
                        chroma_offset_l0.append(chroma_offset_l0_i)
                    else:
                        chroma_weight_l0.append(None)
                        chroma_offset_l0.append(None)
                else:
                    chroma_weight_l0.append(None)
                    chroma_offset_l0.append(None)

            luma_weight_l1 = []
            luma_offset_l1 = []
            chroma_weight_l1 = []
            chroma_offset_l1 = []
            if slice_type % 5 == 1:
                for i in range(num_ref_idx_l1_active_minus1 + 1):
                    luma_weight_l1_flag = stream.u(1)
                    if luma_weight_l1_flag:
                        luma_weight_l1.append(stream.se(1))
                        luma_offset_l1.append(stream.se(1))
                    else:
                        luma_weight_l1.append(None)
                        luma_offset_l1.append(None)

                    if sps.chroma_format_idc != 0:
                        chroma_weight_l1_flag = stream.u(1)
                        if chroma_weight_l1_flag:
                            chroma_weight_l1_i = []
                            chroma_offset_l1_i = []
                            for j in range(2):
                                chroma_weight_l1_i.append(stream.se(1))
                                chroma_offset_l1_i.append(stream.se(1))
                            chroma_weight_l1.append(chroma_weight_l1_i)
                            chroma_offset_l1.append(chroma_offset_l1_i)
                        else:
                            chroma_weight_l1.append(None)
                            chroma_offset_l1.append(None)
                    else:
                        chroma_weight_l1.append(None)
                        chroma_offset_l1.append(None)

        # dec_ref_pic_marking (if applicable)
        adaptive_ref_pic_marking_mode_flag = 0
        no_output_of_prior_pics_flag = 0
        long_term_reference_flag = 0
        difference_of_pic_nums_minus1 = []
        long_term_pic_num = []
        long_term_frame_idx = []
        max_long_term_frame_idx_plus1 = []
        if nal_ref_idc != 0:
            if slice_type_str in ("I", "SI"):
                no_output_of_prior_pics_flag = stream.u(1)
                long_term_reference_flag = stream.u(1)
            else:
                adaptive_ref_pic_marking_mode_flag = stream.u(1)
                if adaptive_ref_pic_marking_mode_flag:
                    memory_management_control_operation = -1
                    while memory_management_control_operation != 0:
                        memory_management_control_operation = stream.ue(1)
                        if memory_management_control_operation in (1, 3):
                            difference_of_pic_nums_minus1.append(stream.ue(1))
                        if memory_management_control_operation == 2:
                            long_term_pic_num.append(stream.ue(1))
                        if memory_management_control_operation in (3, 6):
                            long_term_frame_idx.append(stream.ue(1))
                        if memory_management_control_operation == 4:
                            max_long_term_frame_idx_plus1.append(stream.ue(1))

        # cabac_init_idc (ue-v, if applicable)
        if pps.entropy_coding_mode_flag and (slice_type_str not in ("I", "SI")):  # Not I slice
            cabac_init_idc = stream.ue(1)
        else:
            cabac_init_idc = 0

        # slice_qp_delta (se-v)
        slice_qp_delta = stream.se(1)

        # sp_for_switch_flag (u(1), if applicable) and slice_qs_delta (se-v, if applicable)
        slice_qs_delta = 0
        sp_for_switch_flag = 0
        if slice_type_str in ("SP", "SI"):
            if slice_type_str == "SP":
                sp_for_switch_flag = stream.u(1)
            slice_qs_delta = stream.se(1)

        # disable_deblocking_filter_idc (ue-v, if applicable)
        if pps.deblocking_filter_control_present_flag:
            deblocking_file_pointer_position = stream.tell()
            disable_deblocking_filter_idc = stream.ue(1)
            if disable_deblocking_filter_idc != 1:
                slice_alpha_c0_offset_div2 = stream.se(1)
                slice_beta_offset_div2 = stream.se(1)
            else:
                slice_alpha_c0_offset_div2 = 0
                slice_beta_offset_div2 = 0
        else:
            disable_deblocking_filter_idc = 0
            slice_alpha_c0_offset_div2 = 0
            slice_beta_offset_div2 = 0

        # num_slice_groups_minus1 and slice_group_map_type (if applicable)
        if (
            pps.num_slice_groups_minus1 > 0
            and pps.slice_group_map_type >= 3
            and pps.slice_group_map_type <= 5
        ):
            slice_group_change_cycle = stream.u(1)
        else:
            slice_group_change_cycle = 0

        stream_file_position, bits_left_in_bytes = stream.tell()
        stream.close()
        if pps.entropy_coding_mode_flag:
            slice_end_file_position = (self.fhdl.tell(), 0)
        else:
            slice_end_file_position = (stream_file_position, bits_left_in_bytes)

        header = {
            "slice_start_file_position": slice_start_file_position,
            "slice_end_file_position": slice_end_file_position,
            "slice_size": slice_size,
            "nal_ref_idc": nal_ref_idc,
            "nal_unit_type": nal_unit_type,
            "first_mb_in_slice": first_mb_in_slice,
            "slice_type": slice_type_str,
            "pic_parameter_set_id": pic_parameter_set_id,
            "colour_plane_id": colour_plane_id,
            "frame_num": frame_num,
            "field_pic_flag": field_pic_flag,
            "bottom_field_flag": bottom_field_flag,
            "idr_pic_id": idr_pic_id,
            "pic_order_cnt_lsb": pic_order_cnt_lsb,
            "delta_pic_order_cnt_bottom": delta_pic_order_cnt_bottom,
            "delta_pic_order_cnt_0": delta_pic_order_cnt_0,
            "delta_pic_order_cnt_1": delta_pic_order_cnt_1,
            "redundant_pic_cnt_present_flag": redundant_pic_cnt_present_flag,
            "redundant_pic_cnt": redundant_pic_cnt,
            "direct_spatial_mv_pred_flag": direct_spatial_mv_pred_flag,
            "num_ref_idx_active_override_flag": num_ref_idx_active_override_flag,
            "num_ref_idx_l0_active_minus1": num_ref_idx_l0_active_minus1,
            "num_ref_idx_l1_active_minus1": num_ref_idx_l1_active_minus1,
            "ref_pic_list_reordering": ref_pic_list_reordering,
            "adaptive_ref_pic_marking_mode_flag": adaptive_ref_pic_marking_mode_flag,
            "no_output_of_prior_pics_flag": no_output_of_prior_pics_flag,
            "long_term_reference_flag": long_term_reference_flag,
            "difference_of_pic_nums_minus1": difference_of_pic_nums_minus1,
            "long_term_pic_num": long_term_pic_num,
            "long_term_frame_idx": long_term_frame_idx,
            "max_long_term_frame_idx_plus1": max_long_term_frame_idx_plus1,
            "cabac_init_idc": cabac_init_idc,
            "slice_qp_delta": slice_qp_delta,
            "sp_for_switch_flag": sp_for_switch_flag,
            "slice_qs_delta": slice_qs_delta,
            "disable_deblocking_filter_idc": disable_deblocking_filter_idc,
            "deblocking_file_pointer_position": deblocking_file_pointer_position,
            "slice_alpha_c0_offset_div2": slice_alpha_c0_offset_div2,
            "slice_beta_offset_div2": slice_beta_offset_div2,
            "slice_group_change_cycle": slice_group_change_cycle,
            "luma_log2_weight_denom": luma_log2_weight_denom,
            "chroma_log2_weight_denom": chroma_log2_weight_denom,
            "luma_weight_l0": luma_weight_l0,
            "luma_offset_l0": luma_offset_l0,
            "chroma_weight_l0": chroma_weight_l0,
            "chroma_offset_l0": chroma_offset_l0,
            "luma_weight_l1": luma_weight_l1,
            "luma_offset_l1": luma_offset_l1,
            "chroma_weight_l1": chroma_weight_l1,
            "chroma_offset_l1": chroma_offset_l1,
        }
        header = SliceHeader(**header)
        return header

    def sps_parser(self, slice_size: int) -> SPS:
        """
        This function parses the SPS and returns a SPS dataclass object. Verify this with Section 7.3.2.1.1 Sequence parameter set data syntax of the H.264 standard 08/2021.

        Args:
            slice_size (int): The size of the slice in bytes.

        Returns:
            SPS: A SPS dataclass object containing the sps's fields.
        """
        f = self.fhdl
        sps = {}
        profile_idc = b2i(f.read(1))
        constraint_flag_and_reserved_zero_bits = b2i(f.read(1))  # 6 bits constraint flags + 2 bits reserved
        level_idc = b2i(f.read(1))
        stream = Bitstream(f, slice_size)
        seq_parameter_set_id = stream.ue(1)
        separate_colour_plane_flag = 0
        seq_scaling_list_present_flag = []
        chroma_format_idc = 1
        bit_depth_luma_minus8 = 0
        bit_depth_chroma_minus8 = 0
        qpprime_y_zero_transform_bypass_flag = 0
        seq_scaling_matrix_present_flag = 0
        if profile_idc in [100, 110, 122, 244, 44, 83, 86, 118, 128, 138, 139, 134, 135]:
            chroma_format_idc = stream.ue(1)
            if chroma_format_idc == 3:
                separate_colour_plane_flag = stream.u(1)
            bit_depth_luma_minus8 = stream.ue(1)
            bit_depth_chroma_minus8 = stream.ue(1)
            qpprime_y_zero_transform_bypass_flag = stream.u(1)
            seq_scaling_matrix_present_flag = stream.u(1)
            if seq_scaling_matrix_present_flag:
                for i in range(j := 8 if chroma_format_idc != 3 else 12):
                    seq_scaling_list_present_flag.append(stream.u(1))
                    # TODO: scaling_list stuff not implemeted pg.44
                    if seq_scaling_list_present_flag[i]:
                        if i < 6:
                            # self.scaling_list(16, stream)
                            pass
                        else:
                            # self.scaling_list(64, stream)
                            pass
        log2_max_frame_num_minus4 = stream.ue(1)
        pic_order_cnt_type = stream.ue(1)
        log2_max_pic_order_cnt_lsb_minus4 = 0
        delta_pic_order_always_zero_flag = 0
        offset_for_non_ref_pic = 0
        offset_for_top_to_bottom_field = 0
        num_ref_frames_in_pic_order_cnt_cycle = 0
        offset_for_ref_frame = []
        if pic_order_cnt_type == 0:
            log2_max_pic_order_cnt_lsb_minus4 = stream.ue(1)
        elif pic_order_cnt_type == 1:
            delta_pic_order_always_zero_flag = stream.u(1)
            offset_for_non_ref_pic = stream.se(1)
            offset_for_top_to_bottom_field = stream.se(1)
            num_ref_frames_in_pic_order_cnt_cycle = stream.ue(1)
            offset_for_ref_frame = [stream.se(1) for _ in range(num_ref_frames_in_pic_order_cnt_cycle)]
        max_num_ref_frames = stream.ue(1)
        gaps_in_frame_num_value_allowed_flag = stream.u(1)
        pic_width_in_mbs_minus1 = stream.ue(1)
        pic_height_in_map_units_minus1 = stream.ue(1)
        frame_mbs_only_flag = stream.u(1)
        mb_adaptive_frame_field_flag = 0
        if not frame_mbs_only_flag:
            mb_adaptive_frame_field_flag = stream.u(1)
        direct_8x8_inference_flag = stream.u(1)
        frame_cropping_flag = stream.u(1)
        if frame_cropping_flag:
            frame_crop_left_offset = stream.ue(1)
            frame_crop_right_offset = stream.ue(1)
            frame_crop_top_offset = stream.ue(1)
            frame_crop_bottom_offset = stream.ue(1)
        else:
            frame_crop_left_offset = 0
            frame_crop_right_offset = 0
            frame_crop_top_offset = 0
            frame_crop_bottom_offset = 0
        vui_parameters_present_flag = stream.u(1)  # Annex E vui_parameters()
        stream.close()

        sps = {
            "profile_idc": profile_idc,
            "constraint_flag_and_reserved_zero_bits": constraint_flag_and_reserved_zero_bits,
            "level_idc": level_idc,
            "seq_parameter_set_id": seq_parameter_set_id,
            "chroma_format_idc": chroma_format_idc,
            "separate_colour_plane_flag": separate_colour_plane_flag,
            "bit_depth_luma_minus8": bit_depth_luma_minus8,
            "bit_depth_chroma_minus8": bit_depth_chroma_minus8,
            "qpprime_y_zero_transform_bypass_flag": qpprime_y_zero_transform_bypass_flag,
            "seq_scaling_matrix_present_flag": seq_scaling_matrix_present_flag,
            "log2_max_frame_num_minus4": log2_max_frame_num_minus4,
            "pic_order_cnt_type": pic_order_cnt_type,
            "log2_max_pic_order_cnt_lsb_minus4": log2_max_pic_order_cnt_lsb_minus4,
            "delta_pic_order_always_zero_flag": delta_pic_order_always_zero_flag,
            "offset_for_non_ref_pic": offset_for_non_ref_pic,
            "offset_for_top_to_bottom_field": offset_for_top_to_bottom_field,
            "num_ref_frames_in_pic_order_cnt_cycle": num_ref_frames_in_pic_order_cnt_cycle,
            "offset_for_ref_frame": offset_for_ref_frame,
            "max_num_ref_frames": max_num_ref_frames,
            "gaps_in_frame_num_value_allowed_flag": gaps_in_frame_num_value_allowed_flag,
            "pic_width_in_mbs_minus1": pic_width_in_mbs_minus1,
            "pic_height_in_map_units_minus1": pic_height_in_map_units_minus1,
            "frame_mbs_only_flag": frame_mbs_only_flag,
            "mb_adaptive_frame_field_flag": mb_adaptive_frame_field_flag,
            "direct_8x8_inference_flag": direct_8x8_inference_flag,
            "frame_cropping_flag": frame_cropping_flag,
            "frame_crop_left_offset": frame_crop_left_offset,
            "frame_crop_right_offset": frame_crop_right_offset,
            "frame_crop_top_offset": frame_crop_top_offset,
            "frame_crop_bottom_offset": frame_crop_bottom_offset,
            "vui_parameters_present_flag": vui_parameters_present_flag,
        }
        sps = SPS(**sps)
        self.SPS[seq_parameter_set_id] = sps
        return sps

    def pps_parser(self, slice_size: int) -> PPS:
        """
        This function parses the picture parameter set (PPS) and returns a PPS dataclass object. Verify this with Section 7.3.2.2 of the H.264 standard 08/2021.

        Args:
            slice_size (int): The size of the slice in bytes.

        Returns:
            PPS: A PPS dataclass object containing the pps's fields.
        """
        f = self.fhdl
        pps = {}
        stream = Bitstream(f, slice_size)
        pic_parameter_set_id = stream.ue(1)
        seq_parameter_set_id = stream.ue(1)
        sps = self.SPS[seq_parameter_set_id]
        entropy_coding_mode_flag = stream.u(1)  # 0: CAVLC, 1: CABAC
        bottom_field_pic_order_in_frame_present_flag = stream.u(1)
        num_slice_groups_minus1 = stream.ue(1)
        slice_group_map_type = 0
        run_length_minus1 = 0
        top_left = 0
        bottom_right = 0
        slice_group_change_direction_flag = 0
        slice_group_change_rate_minus1 = 0
        pic_size_in_map_units_minus1 = 0
        slice_group_id = []
        if num_slice_groups_minus1 > 0:
            slice_group_map_type = stream.ue(1)
            if slice_group_map_type == 0:
                run_length_minus1 = stream.ue(num_slice_groups_minus1 + 1)
            elif slice_group_map_type == 2:
                tmp = stream.ue(num_slice_groups_minus1 * 2)
                top_left = tmp[0::2]
                bottom_right = tmp[1::2]
            elif slice_group_map_type in [3, 4, 5]:
                slice_group_change_direction_flag = stream.u(1)
                slice_group_change_rate_minus1 = stream.ue(1)
            elif slice_group_map_type == 6:
                pic_size_in_map_units_minus1 = stream.ue(1)
                slice_group_id = [
                    stream.u(int(np.ceil(np.log2(num_slice_groups_minus1 + 1))))
                    for _ in pic_size_in_map_units_minus1 + 1
                ]
        num_ref_idx_l0_default_active_minus1 = stream.ue(1)
        num_ref_idx_l1_default_active_minus1 = stream.ue(1)
        weighted_pred_flag = stream.u(1)
        weighted_bipred_idc = stream.u(2)
        pic_init_qp_minus26 = stream.se(1)
        pic_init_qs_minus26 = stream.se(1)
        chroma_qp_index_offset = stream.se(1)
        deblocking_filter_control_present_flag = stream.u(1)
        constrained_intra_pred_flag = stream.u(1)
        redundant_pic_cnt_present_flag = stream.u(1)
        transform_8x8_mode_flag = 0
        pic_scaling_matrix_present_flag = 0
        second_chroma_qp_index_offset = 0

        is_more_rbsp_data = False
        if stream.more_data():
            stream.read(stream.bytesleft)
            trailing_bits = stream.stream
            for i in range(1, len(trailing_bits)):
                if trailing_bits[i] == 1:
                    is_more_rbsp_data = True
                    break

        if is_more_rbsp_data:
            transform_8x8_mode_flag = stream.u(1)
            pic_scaling_matrix_present_flag = stream.u(1)
            if pic_scaling_matrix_present_flag:
                # TODO: Implement scaling list parsing page 47
                for i in range(j := 6 + (2 if sps.chroma_format_idc != 3 else 6) * transform_8x8_mode_flag):
                    pic_scaling_list_present_flag = stream.u(1)
                    if pic_scaling_list_present_flag:
                        if i < 6:
                            # self.scaling_list(stream, 16)
                            pass
                        else:
                            # self.scaling_list(stream, 64)
                            pass

            second_chroma_qp_index_offset = stream.se(1)

        stream.close()

        pps = {
            "pic_parameter_set_id": pic_parameter_set_id,
            "seq_parameter_set_id": seq_parameter_set_id,
            "entropy_coding_mode_flag": entropy_coding_mode_flag,
            "bottom_field_pic_order_in_frame_present_flag": bottom_field_pic_order_in_frame_present_flag,
            "num_slice_groups_minus1": num_slice_groups_minus1,
            "slice_group_map_type": slice_group_map_type,
            "run_length_minus1": run_length_minus1,
            "top_left": top_left,
            "bottom_right": bottom_right,
            "slice_group_change_direction_flag": slice_group_change_direction_flag,
            "slice_group_change_rate_minus1": slice_group_change_rate_minus1,
            "pic_size_in_map_units_minus1": pic_size_in_map_units_minus1,
            "slice_group_id": slice_group_id,
            "num_ref_idx_l0_default_active_minus1": num_ref_idx_l0_default_active_minus1,
            "num_ref_idx_l1_default_active_minus1": num_ref_idx_l1_default_active_minus1,
            "weighted_pred_flag": weighted_pred_flag,
            "weighted_bipred_idc": weighted_bipred_idc,
            "pic_init_qp_minus26": pic_init_qp_minus26,
            "pic_init_qs_minus26": pic_init_qs_minus26,
            "chroma_qp_index_offset": chroma_qp_index_offset,
            "deblocking_filter_control_present_flag": deblocking_filter_control_present_flag,
            "constrained_intra_pred_flag": constrained_intra_pred_flag,
            "redundant_pic_cnt_present_flag": redundant_pic_cnt_present_flag,
            "transform_8x8_mode_flag": transform_8x8_mode_flag,
            "pic_scaling_matrix_present_flag": pic_scaling_matrix_present_flag,
            "second_chroma_qp_index_offset": second_chroma_qp_index_offset,
        }
        pps = PPS(**pps)
        self.PPS[pic_parameter_set_id] = pps
        return pps

    def ismp4(self) -> bool:
        """
        Looks for the ftyp atom at the beginning of a file to determine if it is an mp4.

        returns
        True if the second 4 bytes of the file are b'ftyp'
        """
        f = self.fhdl
        ret = f.tell()
        f.seek(0)
        buff = f.read(8)
        f.seek(ret)
        return buff[4:] == b"ftyp"

    def open(self):
        """
        Open the video as a bytes object
        """
        self.fhdl = open(self.fname, "rb+")

    def close(self):
        """
        Close the video
        """
        self.fhdl.close()
