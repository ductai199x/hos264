import math
from bitarray import frozenbitarray

MB_TYPE_I = {
    frozenbitarray("0"): "I_NxN",
    frozenbitarray("100000"): "I_16x16_0_0_0",
    frozenbitarray("100001"): "I_16x16_1_0_0",
    frozenbitarray("100010"): "I_16x16_2_0_0",
    frozenbitarray("100011"): "I_16x16_3_0_0",
    frozenbitarray("1001000"): "I_16x16_0_1_0",
    frozenbitarray("1001001"): "I_16x16_1_1_0",
    frozenbitarray("1001010"): "I_16x16_2_1_0",
    frozenbitarray("1001011"): "I_16x16_3_1_0",
    frozenbitarray("1001100"): "I_16x16_0_2_0",
    frozenbitarray("1001101"): "I_16x16_1_2_0",
    frozenbitarray("1001110"): "I_16x16_2_2_0",
    frozenbitarray("1001111"): "I_16x16_3_2_0",
    frozenbitarray("101000"): "I_16x16_0_0_1",
    frozenbitarray("101001"): "I_16x16_1_0_1",
    frozenbitarray("101010"): "I_16x16_2_0_1",
    frozenbitarray("101011"): "I_16x16_3_0_1",
    frozenbitarray("1011000"): "I_16x16_0_1_1",
    frozenbitarray("1011001"): "I_16x16_1_1_1",
    frozenbitarray("1011010"): "I_16x16_2_1_1",
    frozenbitarray("1011011"): "I_16x16_3_1_1",
    frozenbitarray("1011100"): "I_16x16_0_2_1",
    frozenbitarray("1011101"): "I_16x16_1_2_1",
    frozenbitarray("1011110"): "I_16x16_2_2_1",
    frozenbitarray("1011111"): "I_16x16_3_2_1",
    frozenbitarray("11"): "I_PCM",
}

MB_TYPE_P = {
    frozenbitarray("000"): "P_L0_16x16",
    frozenbitarray("011"): "P_L0_L0_16x8",
    frozenbitarray("010"): "P_L0_L0_8x16",
    frozenbitarray("001"): "P_8x8",
}

MB_TYPE_B = {
    frozenbitarray("0"): "B_Direct_16x16",
    frozenbitarray("100"): "B_L0_16x16",
    frozenbitarray("101"): "B_L1_16x16",
    frozenbitarray("110000"): "B_Bi_16x16",
    frozenbitarray("110001"): "B_L0_L0_16x8",
    frozenbitarray("110010"): "B_L0_L0_8x16",
    frozenbitarray("110011"): "B_L1_L1_16x8",
    frozenbitarray("110100"): "B_L1_L1_8x16",
    frozenbitarray("110101"): "B_L0_L1_16x8",
    frozenbitarray("110110"): "B_L0_L1_8x16",
    frozenbitarray("110111"): "B_L1_L0_16x8",
    frozenbitarray("111110"): "B_L1_L0_8x16",
    frozenbitarray("1110000"): "B_L0_Bi_16x8",
    frozenbitarray("1110001"): "B_L0_Bi_8x16",
    frozenbitarray("1110010"): "B_L1_Bi_16x8",
    frozenbitarray("1110011"): "B_L1_Bi_8x16",
    frozenbitarray("1110100"): "B_Bi_L0_16x8",
    frozenbitarray("1110101"): "B_Bi_L0_8x16",
    frozenbitarray("1110110"): "B_Bi_L1_16x8",
    frozenbitarray("1110111"): "B_Bi_L1_8x16",
    frozenbitarray("1111000"): "B_Bi_Bi_16x8",
    frozenbitarray("1111001"): "B_Bi_Bi_8x16",
    frozenbitarray("111111"): "B_8x8",
    frozenbitarray("111101"): "B_Skip",
}

SUBMB_TYPE_P = {
    frozenbitarray("1"): "P_L0_8x8",
    frozenbitarray("00"): "P_L0_8x4",
    frozenbitarray("011"): "P_L0_4x8",
    frozenbitarray("010"): "P_L0_4x4",
}

SUBMB_TYPE_B = {
    frozenbitarray("0"): "B_Direct_8x8",
    frozenbitarray("100"): "B_L0_8x8",
    frozenbitarray("101"): "B_L1_8x8",
    frozenbitarray("11000"): "B_Bi_8x8",
    frozenbitarray("11001"): "B_L0_8x4",
    frozenbitarray("11010"): "B_L0_4x8",
    frozenbitarray("11011"): "B_L1_8x4",
    frozenbitarray("111000"): "B_L1_4x8",
    frozenbitarray("111001"): "B_Bi_8x4",
    frozenbitarray("111010"): "B_Bi_4x8",
    frozenbitarray("111011"): "B_L0_4x4",
    frozenbitarray("1110"): "B_L1_4x4",
    frozenbitarray("1111"): "B_Bi_4x4",
}


def U():
    """
    status: awaiting testing
    pg 251
    """

    def codebook(query):
        num = len(query)
        if num == 0:
            return
        if query[-1] == 0:
            return num - 1
        else:
            return None

    return codebook


def TU(cmax: int):
    """
    status: awaiting testing
    pg 252
    """
    fixed_len = cmax

    def codebook(query):
        num = len(query)
        if num == 0:
            return None
        if query[-1] == 0:
            return num - 1
        if num == fixed_len:
            return num
        return None

    return codebook


def UEG3():
    """ """

    def codebook(query):
        try:
            ndx = query.index(0)
        except ValueError:
            return None
        if len(query) < (ndx * 2 + 4):
            return None
        base = ((1 << ndx) - 1) << 3
        ret = query[ndx:]
        x = ret.fill()
        ret = int.from_bytes(ret, byteorder="big") >> x
        return base + ret + 9

    return codebook


def UEG0():
    """ """

    def codebook(query):
        try:
            ndx = query.index(0)
        except ValueError:
            return None
        if len(query) < (ndx * 2 + 1):
            return None
        ret = query[ndx:]
        x = ret.fill()
        ret = int.from_bytes(ret, byteorder="big") >> x
        return ret + 14

    return codebook


def UEGk(k, signedValFlag, uCoeff):
    """
    status: awaiting testing
    Looks like only UEG0 and UEG3 are needed.
    This is coded the way I would expect to decode UEG[0,3], not as suggested in the standard.
    Section 9.3.2.3 pg 252
    """
    p_code = TU(cmax=uCoeff)
    if k == 0:
        f1 = 1
        f2 = 1
    elif k == 3:
        f1 = 3
        f2 = 8

    def codebook(query):
        prefix = p_code(query[:uCoeff])
        if prefix is None:
            return None
        if not signedValFlag and not prefix == uCoeff:
            return prefix
        if signedValFlag and query[0] == 0:
            return prefix
        suffix = query[uCoeff:]
        try:
            ndx = suffix.index(0)
        except ValueError:
            return None
        if len(suffix) < ndx * 2 + f1:
            return None
        ret = 0
        for binval in suffix:
            ret <<= 1
            ret += 1 - binval
        if k == 0:
            ret += 13
        return ret - f2

    return codebook


def FL(cmax: int):
    """
    status: awaiting testing
    pg 253
    """
    fixed_len = cmax.bit_length()

    def codebook(query):
        if len(query) < fixed_len:
            return None
        query.fill()
        query.reverse()
        return int(query.to01(), 2)

    return codebook


def MBtype(slice_type):
    """
    status: awaiting testing
    Section 9.3.2.5 pg 253
    """

    def codebook_i(query):
        query = frozenbitarray(query)
        if query in MB_TYPE_I:
            return MB_TYPE_I[query]
        return None

    def codebook_p(query):
        query = frozenbitarray(query)
        if len(query) == 0:
            return None
        if query[0] == 1:
            ret = codebook_i(query[1:])
            return ret
        if query in MB_TYPE_P:
            return MB_TYPE_P[query]
        return None

    def codebook_b(query):
        query = frozenbitarray(query)
        if len(query) == 0:
            return None
        if query[:6] == frozenbitarray("111101"):
            ret = codebook_i(query[6:])
            return ret
        if query in MB_TYPE_B:
            return MB_TYPE_B[query]
        return None

    if slice_type == "I":
        return codebook_i
    if slice_type == "P":
        return codebook_p
    if slice_type == "B":
        return codebook_b
    raise NotImplementedError(f"Slice type {slice_type} is not currently supported")


def sub_MBtype(slice_type):
    """
    status: awaiting testing
    """

    def codebook_subp(query):
        query = frozenbitarray(query)
        if query in SUBMB_TYPE_P:
            return SUBMB_TYPE_P[query]
        return None

    def codebook_subb(query):
        query = frozenbitarray(query)
        if query in SUBMB_TYPE_B:
            return SUBMB_TYPE_B[query]
        return None

    if slice_type == "P":
        return codebook_subp
    if slice_type == "B":
        return codebook_subb
    raise ValueError(f"Unable to handle slice type {slice_type}")


def mb_qp_delta():
    """
    Status: awaiting testing
    Something about table 9-3
    It's just a signed translation layer to unsigned coding
    pg 256
    """
    unsigned = U()

    def codebook(query):
        ret = unsigned(query)
        if ret is not None:
            return -(ret + 1) // 2 + 1 if ret % 2 == 0 else (ret + 1) // 2
        return None

    return codebook
